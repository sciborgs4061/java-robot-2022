// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.


package edu.wpi.first.math.spline;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import org.ejml.simple.SimpleMatrix;

/** Represents a two-dimensional parametric spline that interpolates between two points. */
public class ArcPseudoSpline extends Spline {
  /**
   * Constructs arc segment with the give @start, radius, and amount of turn
   */
  Translation2d m_startPoint;
  Rotation2d m_startRadial;
  Rotation2d m_startHeading;
  double m_radius;
  double m_curvature;
  Rotation2d m_turnBy; // positive - left turn, negative - right turn -- in radians

  SimpleMatrix m_dummyCoeffs = new SimpleMatrix(1,1);

  protected SimpleMatrix getCoefficients() { return m_dummyCoeffs; }


  public ArcPseudoSpline(Pose2d start, double radius, double turnBy) {
    super(1);
    m_startHeading = start.getRotation();
    if (turnBy*radius >= 0) { // left turn; radial angle is heading angle - 90 deg.
      m_startRadial = m_startHeading.minus(new Rotation2d(0,1));
    } else { // right turn; radial angle is heading angle + 90 deg.
      m_startRadial = m_startHeading.plus(new Rotation2d(0,1));
    }
    m_startPoint = start.getTranslation();
    m_radius = radius;
    m_curvature = Math.signum(turnBy) * 1/radius;
    m_turnBy = new Rotation2d(turnBy);
  }


  /**
   * Gets the pose and curvature at some point t on the spline.
   *
   * @param t The point t
   * @return The pose and curvature at that point.
   */
  @SuppressWarnings("ParameterName")
  public PoseWithCurvature getPoint(double t) {
    Rotation2d deltaTheta = m_turnBy.times(t);
    Rotation2d newTheta = m_startRadial.plus(deltaTheta);
    Translation2d deltaPos = new Translation2d(m_radius*(newTheta.getCos()-m_startRadial.getCos()), 
                                               m_radius*(newTheta.getSin()-m_startRadial.getSin())).times(Math.signum(m_radius));
    //Translation2d deltaPos = new Translation2d(deltaTheta.getRadians()*m_radius, 0);
    return new PoseWithCurvature(new Pose2d(m_startPoint.plus(deltaPos), m_startHeading.plus(deltaTheta)), m_curvature);
  }
}