// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package edu.wpi.first.math.spline;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;

import org.ejml.simple.SimpleMatrix;

/** Represents a two-dimensional parametric spline that interpolates between 
 *  a point on an offset tangent and a circle of radius arriving at the circle after traveling distance
 *  s and two points.
 *  If direction is -1, the interpolation is from a circle of radius r with a transition
 *  of radius s.
 *  The robot should be placed at the start position by previous elements of the trajectory. 
 * */
public class ClothoidPseudoSpline extends Spline {
  /**
   * Constructs arc segment with the give @start, radius, and amount of turn
   */
  Translation2d m_anchorPoint;
  Rotation2d m_anchorHeading;
  Rotation2d m_referenceHeading;
  double m_radius; // negative for right turns, positive for left turns 
  double m_distance;
  double m_entering;   // 1.0 - entering the circle; -1.0 - leaving the circle
  double m_turnDirection; // 1.0 - left, -1.0 right
  Clothoid m_clothoid;
  
  SimpleMatrix m_dummyCoeffs = new SimpleMatrix(1,1);

  protected SimpleMatrix getCoefficients() { return m_dummyCoeffs; }


  public ClothoidPseudoSpline(Pose2d anchor, double radius, double distance, boolean entering) {
    super(1);
    m_anchorHeading = anchor.getRotation();
    m_referenceHeading = entering ? anchor.getRotation() : anchor.getRotation().plus(Rotation2d.fromDegrees(180));
    m_anchorPoint = anchor.getTranslation();
    m_turnDirection = Math.signum(radius);
    m_radius = Math.abs(radius);
    m_distance = distance;
    m_entering = entering ? 1.0  : -1.0;
    m_clothoid = new Clothoid(m_distance, m_radius);
  }

  static double C (double s) {
    /* polynomial approximation to normalized Euler Spiral fresnel cosine integral
     * probably bad beyond s=1 or so
     */
    return s - Math.pow(s,5)/10;
  }
  static double S (double s) {
    /* polynomial approximation to normalized Euler Spiral fresnel sine integral
     * probably bad beyond s=1 or so
     */
    return Math.pow(s,3)/3 - Math.pow(s,7)/42;
  }
  // Clothoid calculates the position, curvature, and angle turned for clothoid
  // curves anchored at (0,0) with heading 0, assuming starting at (0,0) with
  // heading 0 and turning left. The caller is responsible for adjustments related to:
  //   * turning right
  //   * traveling the curve backward (toward the anchor point)
  //   * starting at a position other than (0,0)
  //   * starting at a non-zero heading

  class Clothoid {
    private final double m_scale;
    private final double m_radius;
    private final double m_distance;
    public Clothoid (double distance, double radius) {
      m_scale = Math.sqrt(2*distance*radius);
      m_radius = radius;
      m_distance = distance;
    }

    private Translation2d getCoords(double s) {
      return new Translation2d (m_scale*(C(s*m_distance/m_scale)),
                                m_scale*(S(s*m_distance/m_scale))
                               );
    }
    private double getCurvature(double s) {
      return s/m_radius; 
    }
    private Rotation2d getAngle (double s) {
      s = m_distance*s;
      return new Rotation2d(s*s/(m_scale*m_scale));
    }
  
  }

  private Translation2d mirrorY(Translation2d trans) {
    return new Translation2d(trans.getX(), -trans.getY());
  }
  private double orient(double s) { return (m_entering > 0 ? s : 1-s);}
  /**
   * Gets the pose and curvature at some point t on the spline.
   *
   * @param t The point t
   * @return The pose and curvature at that point.
   * Danger: the parameter is likely already scaled 0 to 1, hence
   * the parameter scaling in getCoords above is likely wrong!
   * Only works for initial heading 0 at the moment
   */
  @SuppressWarnings("ParameterName")
  public PoseWithCurvature getPoint(double t) {
    // Get the answers for a standard clothoid anchored at (0,0), entering, with heading 0, and turning left
    double s = orient(t);
    Translation2d deltaPos = m_clothoid.getCoords(s); 
    Rotation2d deltaTheta = m_clothoid.getAngle(s);
    double curvature = m_clothoid.getCurvature(s);
    // Transform them for the current situation
    // Right turns always have negative curvature
    curvature = m_turnDirection * curvature;
    // Delta theta is relative to the anchor heading
    //   * positive for left entering and right departing turns
    //   * negative for the others
    deltaTheta = m_entering*m_turnDirection >0 ? deltaTheta : deltaTheta.times(-1.0);
    // Y coordinate is 
    //   * positive for left entering and right departing turns
    //   * negative for the others
    deltaPos = m_entering*m_turnDirection >0 ? deltaPos : mirrorY(deltaPos);
    // deltaPos needs to be further adjusted by rotating to the reference heading
    deltaPos = deltaPos.rotateBy(m_referenceHeading);



    return new PoseWithCurvature(new Pose2d(m_anchorPoint.plus(deltaPos), 
                                            m_anchorHeading.plus(deltaTheta)), 
                                            curvature);
  }
}