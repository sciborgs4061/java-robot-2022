// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package edu.wpi.first.math.spline;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import org.ejml.simple.SimpleMatrix;

/** Represents a two-dimensional parametric spline that interpolates between two points. */
public class LinearPseudoSpline extends Spline {
  /**
   * Constructs line segment with the give @start and @end
   */
  Translation2d m_start;
  Rotation2d m_heading;
  Translation2d m_diff;
  SimpleMatrix m_dummyCoeffs = new SimpleMatrix(1,1);

  protected SimpleMatrix getCoefficients() { return m_dummyCoeffs; }

  public LinearPseudoSpline(Translation2d start, Translation2d end) {
    super(1);
    m_start = start;
    m_diff = end.minus(m_start);
    m_heading = new Rotation2d(m_diff.getX(), m_diff.getY());
  }

  public LinearPseudoSpline(Pose2d start, double distance) {
    super(1);
    m_start = start.getTranslation();
    m_heading = start.getRotation();
    m_diff = new Translation2d(distance*m_heading.getCos(), distance*m_heading.getSin());
  }
  
  /**
   * Gets the pose and curvature at some point t on the spline.
   *
   * @param t The point t
   * @return The pose and curvature at that point.
   */
  @SuppressWarnings("ParameterName")
  public PoseWithCurvature getPoint(double t) {
    Translation2d delta = m_diff.times(t); 
    // Curvature is always 0 for a line
    return new PoseWithCurvature(new Pose2d(m_start.plus(delta), m_heading), 0);
  }
}