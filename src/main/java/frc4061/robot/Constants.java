/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc4061.robot;

import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import edu.wpi.first.math.kinematics.DifferentialDriveKinematics;
import edu.wpi.first.math.trajectory.TrajectoryConfig;
import edu.wpi.first.math.trajectory.constraint.DifferentialDriveVoltageConstraint;
import edu.wpi.first.math.util.Units;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide
 * numerical or boolean constants. This class should not be used for any other
 * purpose. All constants should be declared globally (i.e. public static). Do
 * not put anything functional in this class.
 *
 * <p>
 * It is advised to statically import this class (or one of its inner classes)
 * wherever the constants are needed, to reduce verbosity.
 */
public final class Constants {
    public static final double kRobotPeriod = 0.02;
    
    public final static class Drivebase {
        public static final double kRobotLengthInches = 38;
        public static final double kDefaultSlewRateLimit = 1.4;
        public static final double kMass = 65; // kg - guess
        public static final double kMOI = 0.862; // wild guess
        public static final double kGearRatio = 11.3/1; // per Steve W. on 1/30/22
        public static final double kWheelDiameter = Units.inchesToMeters(6);
        public static final double kNominalTrackWidthMeters = Units.inchesToMeters(21.875); // ~0.55
        public static final double kSpeedDeadband = 0.05; // deadband to use arcade drive instead of curvature drive
        public static final double kTurnDeadband = 0.05;
        // The Falcon 500 encoders report 2048 counts per revolution
        public static final double kDistancePerEncoderCount = (3.14 * kWheelDiameter / kGearRatio) / 2048;
        public static final byte kGyroUpdateRateHz = 50; // matches robot period of 20ms

        // constants from frc-characterization for the simulated robot
        // test run on 1/31/22.

        private static final double sim_ksVolts = 0.0046;  
        private static final double sim_kvVoltSecondsPerMeter = 2.646;
        private static final double sim_kaVoltSecondsSquaredPerMeter = 0.184;
        private static final double sim_rSquared = 1.0;
        private static final double sim_kTrackWidthMeters = 0.964;
        private static final double sim_kPDriveVel = 0.7; // originally 2.8
        private static final double sim_kD = 0;

        // Robot constants from SysId run on 3/9/22
        // Angular characterization not run because NavX not working.
        // Empirical track width not available due to no angular characterization
        // Kp from SysId is 8.75 which seems ridiculously large
        // 

        public static final double real_ksVolts = 0.66;
        public static final double real_kvVoltSecondsPerMeter = 2.47;
        public static final double real_kaVoltSecondsSquaredPerMeter = 0.25; // this is a complete guess; data aren't good
        public static final double real_rSquared = 0.96;
        public static final double real_kTrackWidthMeters = 0.572; // this is a complete guess; data aren't good
        public static final double real_kPDriveVel = 0.2; // halve this if too jerky; halve again if still too jerky
        public static final double real_kD = 0;

        private static boolean isReal = Robot.isReal();
        public static final double ksVolts = isReal ? real_ksVolts : sim_ksVolts;
        public static final double kvVoltSecondsPerMeter = isReal ? real_kvVoltSecondsPerMeter
                : sim_kvVoltSecondsPerMeter;
        public static final double kaVoltSecondsSquaredPerMeter = isReal ? real_kaVoltSecondsSquaredPerMeter
                : sim_kaVoltSecondsSquaredPerMeter;
        public static final double rSquared = isReal ? real_rSquared : sim_rSquared;
        public static final double kTrackWidthMeters = isReal ? real_kTrackWidthMeters : sim_kTrackWidthMeters;
        public static final double kPDriveVel = isReal ? real_kPDriveVel : sim_kPDriveVel;
        public static final double kD = isReal ? real_kD : sim_kD;

        // instances encapsulating the above
        public static final DifferentialDriveKinematics kDriveKinematics = new DifferentialDriveKinematics(
                kTrackWidthMeters);
        public static double kMaxSpeedMetersPerSecond = 2.60;
        public static double kMaxAccelerationMetersPerSecondSquared = 1.45;
        // best settings for barrel - speed 2.35, accel 1.5, kP = 0.105
        // best settings for bounce - speed 2.75, accel 2.0, dP = 0.105
        // best so far for slalom: 2.35, accel 1.25, kP 0.105

        // RAMSETE follower parameters - not robot specific
        public static final double kRamseteB = 2;
        public static final double kRamseteZeta = 0.7;

        // Constraints and configs related to drivebase constants
        public static final DifferentialDriveVoltageConstraint autoVoltageConstraint = new DifferentialDriveVoltageConstraint(
                new SimpleMotorFeedforward(ksVolts,
                        kvVoltSecondsPerMeter,
                        kaVoltSecondsSquaredPerMeter),
                kDriveKinematics, 6);
        public static final DifferentialDriveVoltageConstraint highVoltageConstraint = new DifferentialDriveVoltageConstraint(
                new SimpleMotorFeedforward(ksVolts, kvVoltSecondsPerMeter, kaVoltSecondsSquaredPerMeter),
                kDriveKinematics, 8);
        public static final TrajectoryConfig configForward = new TrajectoryConfig(kMaxSpeedMetersPerSecond,
                kMaxAccelerationMetersPerSecondSquared)
                        // Add kinematics to ensure max speed is actually obeyed
                        .setKinematics(kDriveKinematics)
                        // Apply the voltage constraint
                        .addConstraint(autoVoltageConstraint);
        public static final TrajectoryConfig configForwardNoStop = new TrajectoryConfig(kMaxSpeedMetersPerSecond,
                kMaxAccelerationMetersPerSecondSquared)
                        // Add kinematics to ensure max speed is actually obeyed
                        .setKinematics(kDriveKinematics)
                        // Apply the voltage constraint
                        .addConstraint(autoVoltageConstraint)
                        .setEndVelocity(1.9);
        public static final TrajectoryConfig configFastForward = new TrajectoryConfig(3.6,
                4)
                        // Add kinematics to ensure max speed is actually obeyed
                        .setKinematics(kDriveKinematics)
                        // Apply the voltage constraint
                        .addConstraint(highVoltageConstraint)
                        .setStartVelocity(1.9);
        public static final TrajectoryConfig configStraightForward = new TrajectoryConfig(3.6,
                4)
                        // Add kinematics to ensure max speed is actually obeyed
                        .setKinematics(kDriveKinematics)
                        // Apply the voltage constraint
                        .addConstraint(highVoltageConstraint);
        public static final TrajectoryConfig configStraightReversed = new TrajectoryConfig(3.6,
                4)
                        // Add kinematics to ensure max speed is actually obeyed
                        .setKinematics(kDriveKinematics)
                        // Apply the voltage constraint
                        .addConstraint(highVoltageConstraint)
                        .setReversed(true);

        public static final TrajectoryConfig configReversed = new TrajectoryConfig(kMaxSpeedMetersPerSecond,
                kMaxAccelerationMetersPerSecondSquared)
                        // Add kinematics to ensure max speed is actually obeyed
                        .setKinematics(Constants.Drivebase.kDriveKinematics)
                        // Apply the voltage constraint
                        .addConstraint(autoVoltageConstraint)
                        .setReversed(true);
        public static final TrajectoryConfig configReversedNoStop = new TrajectoryConfig(kMaxSpeedMetersPerSecond,
                kMaxAccelerationMetersPerSecondSquared)
                        // Add kinematics to ensure max speed is actually obeyed
                        .setKinematics(kDriveKinematics)
                        // Apply the voltage constraint
                        .addConstraint(autoVoltageConstraint)
                        .setEndVelocity(1.9)
                        .setReversed(true);
    }

    public final static class ShooterConstants {
        // The shooter has 2 flywheels and two motors geared together
        // such that the speed of the flywheels is equal. 
        // Flywheel constants for one side
        public static final double kReduction = 1.83; // input turns over output turns
        public static final double kMI = 0.006; // moment of inertia - kg M^2 - estimate 1/28/2022
        // public static final double kVoltPerRPM = (12.0/5676.0) * kReduction; // estimated value; RPM's of the output shaft
        public static final double kVoltPerRPM = 0.0032657; // characterization on 3/5/2022
        public static final double kSVolt = 0.53233; // characterization on 3/5/2022
        // public static final double kVoltPerRPM = 2.0/1000; // experiment for
        // robustness
        public static final double kInitialRPMSetpoint = 0; // Wheel, not motor, RPM
        public static final double kInitialVoltageSetpoint = 6.6; // Default voltage requested by driveteam
        public static final double kFeederOutputSetpoint = 0.4; // 0-1.0 -- fraction of 12 volts
        public static final double kP = 0.005; // TODO: Adjust further, empirically
        public static final double kI = 0.0;
        public static final double kD = 0;
        public static final double kMaxStep = 7; // max amount new voltage can exceed current voltage
        // turns CW looking at the shaft end (opposite direction from lower motor)
        public static final double statorLimit = 35;
        // TalonFX (Falcon 500) has 2048 ticks per revolution and reports
        // velocity in ticks per 100ms.
        public static final double sensorVelocityPerMotorRPS = 204.8;
        public static final double sensorVelocityPerMotorRPM = sensorVelocityPerMotorRPS/60;
        public static final double sensorVelocityPerWheelRPM = sensorVelocityPerMotorRPM*kReduction;
    }  
    
    public final static class Limelight {
        // Limelight support
    
        public static final double cameraHeight = 0.8763;
        public static final double targetHeight = 2.6416; 
        public static final double cameraAngle = 0;
        public static final String ipAddress = "10.40.61.11";
    }

    public final static class Turret {
        public static final double azimuthGearRatio = 90.91/1;
        public static final double elevationGearRatio = (4*137.5)/1; // 550
        public static final double neo550TicksPerRevolution = 1; // Sparkmax native reporting is in revolutions
        public static final double azimuthTicksPerDegree = 
            azimuthGearRatio * neo550TicksPerRevolution / 360; 
        public static final double elevationTicksPerDegree = 
            elevationGearRatio * neo550TicksPerRevolution / 360;
        public static final double speed = 0.2; // fraction 0-1 of full motor power
                                                // currently used for both elevation and azimuth

        // TODO: find kP values
        public static final double azimuthKP = 0.01;
        public static final double elevationKP = 0.01;
    }

    public final static class ClimberConstants {
        // Starting configuration absolute readings for climber encoders
        // These are only meaningful if the reset method on the encoders 
        // has NOT been called. (These are absolute: no sign inversion)
        // TODO: fine tune this 
        public static double startPosition = 0.441; // Actual left encoder value
        // 0.003 corresponds to position accuracy of about +/- 1 degree.
        public static double startTolerance = 0.003;
        /* 
         * NOTE: these notes are NOT correct for the climber now due to having
         *     rotated the arms 180 deg.
         *   The encoder absolute values are fraction of a complete circle: i.e. 0-1
         *   The forward limit is vertical upwards (+0.5 turn from vertical downward)
         *   The backward limit is -0.383 from vertical downward
         *   These translate to: +0.333 from the start position for forward
         *   and -0.55 from the start position for backward
         * 
         *   With the arms horizontal (measured 0 deg. on each), the
         *   left absolute reading is 0.269 and the right is 0.254.
         */
        // Based on above observations a correction should be added to
        // the right-side absolution position to make it match the left side.
        // The correction includes terms for both slight position misalignment of the
        // encoder mountings and rotation of the encoder socket itself in 1/6 turn increments
        public static double addToRightAbsPosition = 0.015 + -0.167; // Encoders currently installed 1/6 turn out of phase;
        // The homing speed should be adjust so that homing works quickly
        // enough but reliably achieves "sufficiently accurate" home positions.
        // TODO: Confirm that the homingSpeed works; adjust as needed.
        public static double homingSpeed = 0.15;                 
    }
    
    public final static class Collector {
        public static final double rejectSpeed = 0.25;       // fractional motor power
        public static final double collectSpeed = 0.75;      // fractional motor power
        public static final double delayAfterExtend = 0.25; // seconds

    }
    public final static class Conveyor {
        public static final double conveyorSpeed = 0.5;
    }
}
