package frc4061.robot.utilities;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;


public class SmartDashboardUtil {

    /**
     * Configures a smartdashboard number that tries to first set 
     * itself to a previous existance on smartdashboard otherwise uses defaultVal.
     */
    public static final void ensureNumber(String key, double defaultVal) {
        SmartDashboard.putNumber(
            key,
            SmartDashboard.getNumber(key, defaultVal)
        );
    }

    /**
     * Configures a smartdashboard boolean that tries to first set 
     * itself to a previous existance on smartdashboard otherwise uses defaultVal.
     */
    public static final void ensureBoolean(String key, boolean b) {
        SmartDashboard.putBoolean(
            key,
            SmartDashboard.getBoolean(key, b)
        );
    }  
}
