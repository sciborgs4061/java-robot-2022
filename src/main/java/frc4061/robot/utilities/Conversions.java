package frc4061.robot.utilities;

import edu.wpi.first.math.Pair;

public class Conversions {
    public static String twoToString(double first, double second, String sep) {
        return first + sep + " | " + second + sep;
    }
    public static String pairToString(Pair<Double, Double> nums, String sep) {
        return twoToString(nums.getFirst(), nums.getSecond(), sep);
    }
}
 