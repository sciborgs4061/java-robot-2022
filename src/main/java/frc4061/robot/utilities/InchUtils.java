package frc4061.robot.utilities;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.spline.ArcPseudoSpline;
import edu.wpi.first.math.spline.ClothoidPseudoSpline;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.util.Units;

public class InchUtils {
    public static final Translation2d translation2d(double x, double y) {
        return new Translation2d (Units.inchesToMeters(x), 
                          Units.inchesToMeters(y));
    }

    public static final Rotation2d rotation2d(double theta) {
        return Rotation2d.fromDegrees(theta);
    }

    public static final Pose2d pose2d(double x, double y, double theta) {
        return new Pose2d(translation2d(x,y),
                          rotation2d(theta));
    }

    public static final ArcPseudoSpline arcPseudoSpline(Pose2d start, double radius, double theta) {
        return new ArcPseudoSpline(start, Units.inchesToMeters(radius), Units.degreesToRadians(theta));
    }

    public static final ClothoidPseudoSpline clothoidPseudoSpline(Pose2d start, double radius, 
                                                                  double distance, boolean entering) {
         return new ClothoidPseudoSpline(start, Units.inchesToMeters(radius),
                                         Units.inchesToMeters(distance), entering);                                                               
    }
    
}
