/*******************************************************************
 * Operator Interface Devices and Access Methods
 */

package frc4061.robot;

import edu.wpi.first.math.filter.SlewRateLimiter;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Joystick;
import frc4061.robot.utilities.*;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.button.Button;
import edu.wpi.first.wpilibj2.command.button.POVButton;
import frc4061.robot.Components.EnableSwitchWithOverride;
import frc4061.robot.utilities.SmartDashboardUtil;


public final class DriveteamInterface {

    // Public methods for accessing OperatorInterface devices
    public static final boolean drivebaseEnabled() {
        return Driverstation.m_drivebaseEnable.getEnabled();
    }

    public static final boolean shooterEnabled() {
        return Driverstation.m_shooterEnable.getEnabled();
    }

    public static final boolean collectorEnabled() {
        return Driverstation.m_collectorEnable.getEnabled();
    }
    
    public static final boolean climberEnabled() {
        return Driverstation.m_climberEnable.getEnabled();
    }

    public static final boolean conveyorEnabled() {
        return Driverstation.m_conveyorEnable.getEnabled();
    }

    public static final double getSpeed() {
        return Driver.getSpeed();
    }

    public static double getTurn() {
        return Driver.getTurn();
    }

    public static final boolean conveyorAutoFeedEnabled() {
        return Driverstation.conveyorAutoFeedEnable();
    }
    
    public static boolean getCameraFlipped() {
        return Driverstation.getCameraFlipped();
    }

    public static final void initialize() {
        // ensure that Smartdashboard entries are instantiated
        Driver.initialize();
        Operator.initialize();
        Driverstation.initialize();
    }

    public static final void update() {
        // ensure that Smartdashboard entries are updated
        Driver.update();
        Operator.update();
        Driverstation.update();
    }

    // Driver devices
    public static class Driver {
         
        private static final int driverGameControllerId = 0;
        private static final int speedAxis = 1; // left stick Y
        private static final int turnAxis = 4;  // right stick X
        private static final int turboAxis = 3;
        private static final int antiTurboAxis = 2;
        private static double m_driveBaseLimit = Constants.Drivebase.kDefaultSlewRateLimit;
        public static final XboxController driverXbox = new XboxController(driverGameControllerId);
        private static final String slewRateLimiterKey = "Drivebase/Slew Rate Limit";
        public static SlewRateLimiter m_slewRateLimiter = new SlewRateLimiter(Constants.Drivebase.kDefaultSlewRateLimit);


        // Buttons - mappings from Confluence on 2/19/22
        // These are set up to bind to commands rather than use directly
        public static final Button m_rejectBallButton = new Button(() -> driverXbox.getAButton());
        public static final Button m_conveyorSlowButton = new Button(() -> driverXbox.getBButton());
        public static final Button m_climberForwardButton = new Button(() -> driverXbox.getXButton());
        public static final Button m_climberBackwardButton = new Button(() -> driverXbox.getYButton());
        public static final Button m_povUpButton = new POVButton(driverXbox, 0);
        public static final Button m_povRightButton = new POVButton(driverXbox, 90);
        public static final Button m_povDownButton = new POVButton(driverXbox, 180);
        public static final Button m_povLeftButton = new POVButton(driverXbox, 270);
        public static final Button m_collectBallButton = new Button(() -> driverXbox.getRightBumper());
        public static final Button m_toggleCollectorButton = new Button(() -> driverXbox.getLeftBumper());


        public Driver() {
        }
        public static boolean notInDeadBand (double joystickInput) {
            return joystickInput < -Constants.Drivebase.kSpeedDeadband || joystickInput > Constants.Drivebase.kSpeedDeadband;
        }
        private static double getRateLimitedOutput(double proposedOutput, SlewRateLimiter slewRateLimiter) {
            return slewRateLimiter.calculate(proposedOutput);
        }
        private static final double getSpeed() {
            double joystickInput = (driverXbox.getRawAxis(speedAxis));
            if(notInDeadBand((driverXbox.getRawAxis(speedAxis)))){
                if(driverXbox.getRawAxis(speedAxis) > 0){
                    joystickInput = (driverXbox.getRawAxis(speedAxis) - Constants.Drivebase.kSpeedDeadband) * (1 / (1 - Constants.Drivebase.kSpeedDeadband));
                } else if(driverXbox.getRawAxis(speedAxis) < 0){
                    joystickInput = (driverXbox.getRawAxis(speedAxis) + Constants.Drivebase.kSpeedDeadband) * (1 / (1 - Constants.Drivebase.kSpeedDeadband));
                } else {
                    joystickInput = 0; 
                }
            } 
            double proposedOutput;
            if(notInDeadBand(joystickInput)){
                if (getAntiTurbo()) {
                    proposedOutput = (-joystickInput * 0.80) * 0.60;
                } else if (getTurbo()) {
                    proposedOutput = (-joystickInput * 0.80) * 1.5; 
                } else {
                    proposedOutput = -joystickInput * 0.90;
                }
            } else {
                proposedOutput = 0;
            }
            return getRateLimitedOutput(proposedOutput, m_slewRateLimiter);
        }

        public static final double getTurn() {
            if (getAntiTurbo()) {
                return driverXbox.getRawAxis(turnAxis) * 0.33; 
            } else if (getTurbo()) {
                return driverXbox.getRawAxis(turnAxis) * 0.6;
            } else {
                return driverXbox.getRawAxis(turnAxis) * 0.55;
            }
        }
      
        private static final boolean getTurbo() {
            return (driverXbox.getRawAxis(turboAxis) > 0.5);
        }

        private static final boolean getAntiTurbo() {
            return (driverXbox.getRawAxis(antiTurboAxis) > 0.5);
        }

        private static final void initialize() {
            SmartDashboardUtil.ensureNumber(slewRateLimiterKey, Constants.Drivebase.kDefaultSlewRateLimit);
        }

        private static final void update() {
            double driveBaseLimit = SmartDashboard.getNumber(slewRateLimiterKey, Constants.Drivebase.kDefaultSlewRateLimit);
            if (m_driveBaseLimit != driveBaseLimit) {
                m_driveBaseLimit = driveBaseLimit;
                m_slewRateLimiter = new SlewRateLimiter(m_driveBaseLimit);
                
            }
        }
    }

    // Operator devices
    public static class Operator {
        private static final int operatorGameControllerId = 1;
        private static final int climberSpeedAxis = 1; // left stick Y
        private static final int rightClimberOnlyAxis = 5;  // right stick Y
        private static final int turboAxis = 3;
        private static final int antiTurboAxis = 2;
        public static final XboxController operatorXbox = new XboxController(operatorGameControllerId);

        // Left stick Y axis controls both arms
        // Right stick Y axis controls only right arm
        // Very much dispacement of either stick inhibits the other
        // Remember: Joystick Y-axis values are negative when pushed forward
        public static final double getClimberSpeed() {
            if (operatorXbox.getRawAxis(Math.abs(rightClimberOnlyAxis)) > 0.1) return 0;
            return -operatorXbox.getRawAxis(climberSpeedAxis);
        }

        public static final double getRightOnlyClimberSpeed() {
            if (operatorXbox.getRawAxis(Math.abs(climberSpeedAxis)) > 0.1) return 0;
            return -operatorXbox.getRawAxis(rightClimberOnlyAxis);
        }

        public static final Button m_rejectBallButton = new Button(() -> operatorXbox.getBackButton());
        public static final Button m_collectBallButton = new Button(() -> operatorXbox.getRightBumper());
        public static final Button m_shootButton = new Button(() -> operatorXbox.getYButton());
        public static final Button m_alignShooterButton = new Button(() -> operatorXbox.getAButton());
        public static final Button m_spinOutCollectorButton = new Button(() -> operatorXbox.getLeftBumper());
        public static final Button m_fenderAimButton = new Button(() -> operatorXbox.getBButton());
        public static final Button m_elevationUpButton = new POVButton(operatorXbox, 0);
        public static final Button m_azimuthRightButton = new POVButton(operatorXbox, 90);
        public static final Button m_elevationDownButton = new POVButton(operatorXbox, 180);
        public static final Button m_azimuthLeftButton = new POVButton(operatorXbox, 270);
        
        private static final void initialize() {}
        private static final void update () {}
 
    }

    // Driverstation switchbox (different from both the operator and driver joysticks!)
    static class Driverstation {
        private static final int driverstationJoystickId = 2; // actually, just a switchbox
        public static final Joystick switchbox = new Joystick(driverstationJoystickId);

        // Names for switches on the switchbox
        private static final int climberEnableSwitchId = 1; // toggle switch; button/switch indexes begin with 1     
        private static final int flipCameraSwitchId = 2;    // toggle switch
        private static final int incrementShooterId = 3;    // pushbutton
        private static final int decrementShooterId = 4;    // pushbutton
        private static final int climberBrakeToggle = 5;    // pushbutton
        private static final int homeAllSubsystems = 6;     // pushbutton 
        // id 7 no switch
        // id 8 no switch
        private static final int conveyorAutoFeedEnableSwitchId = 9; // toggle
        private static final int collectorEnableSwitchId = 10; // toggle
        private static final int shooterEnableSwitchId = 11;   // toggle
        private static final int drivebaseEnableSwitchId = 12; // toggle


        private static final String driverstationKey = "Driverstation/";
        public static final Button m_incrementShooter = new Button(() -> switchbox.getRawButton(incrementShooterId));
        public static final Button m_decrementShooter = new Button(() -> switchbox.getRawButton(decrementShooterId));
        public static final EnableSwitchWithOverride m_enableConveyorAutoFeed = new EnableSwitchWithOverride("Conveyor AutoFeed Enabled", switchbox, conveyorAutoFeedEnableSwitchId, false, false);
        public static final Button m_climberBrakeToggle = new Button(() -> switchbox.getRawButton(climberBrakeToggle));
        public static final Button m_homeAllSubsystems = new Button(() -> switchbox.getRawButton(homeAllSubsystems));
        public static final boolean getCameraFlipped() {
            return switchbox.getRawButton(flipCameraSwitchId);
        }
        private static final boolean conveyorAutoFeedEnable() {
            return switchbox.getRawButton(conveyorAutoFeedEnableSwitchId); 
        }

        static EnableSwitchWithOverride m_drivebaseEnable; 
        static EnableSwitchWithOverride m_shooterEnable;
        static EnableSwitchWithOverride m_collectorEnable;
        static EnableSwitchWithOverride m_climberEnable;
        static EnableSwitchWithOverride m_conveyorEnable;
        static EnableSwitchWithOverride m_conveyorAutoFeedEnable;
        static final void initialize() {
            m_drivebaseEnable = new EnableSwitchWithOverride(driverstationKey + "Drivebase", switchbox, 
                drivebaseEnableSwitchId, false, false);
            m_shooterEnable = new EnableSwitchWithOverride(driverstationKey + "Shooter", switchbox, 
                shooterEnableSwitchId, false, false);
            m_collectorEnable = new EnableSwitchWithOverride(driverstationKey + "Collector", switchbox, 
                collectorEnableSwitchId, false, false);
            m_climberEnable = new EnableSwitchWithOverride(driverstationKey + "Climber", switchbox, 
                climberEnableSwitchId, false, false); 
            m_conveyorEnable = new EnableSwitchWithOverride( driverstationKey + "Conveyor", switchbox,
                collectorEnableSwitchId, false, false);
            m_conveyorAutoFeedEnable =  new EnableSwitchWithOverride( driverstationKey + "Conveyor Auto Feed", switchbox,
                conveyorAutoFeedEnableSwitchId, false, false);
        }

        private static final void update() {
            // Get current switch and override states for all the subsystems
            m_drivebaseEnable.update();
            m_shooterEnable.update();
            m_collectorEnable.update();
            m_climberEnable.update();
            m_conveyorEnable.update();
            m_conveyorAutoFeedEnable.update();
        }
    }
    
    static class Robot{
        private static DigitalInput m_debugDio = new DigitalInput(9);
        public static Button m_debugButton = new Button(() -> !m_debugDio.get());
    }
}
