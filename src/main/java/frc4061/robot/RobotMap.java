/////////////////////////////////////////////////////////////////////
// This file stores the various ids/ports used by the robot 
/////////////////////////////////////////////////////////////////////

package frc4061.robot;

import edu.wpi.first.wpilibj.SPI;

public class RobotMap {
    // Drivebase motor controllers (CANBUS - TalonFX)
    public static final Integer[] LEFT_MOTOR_IDS = { 3, 4, 5 };
    public static final Integer[] RIGHT_MOTOR_IDS = { 6, 7, 8 };

    // Collector solenoids pneumatics
    public static final int COLLECTOR_IN = 1;
    public static final int COLLECTOR_OUT = 0;

    // Shooter motor controllers (CANBUS - TalonFX)
    public static final int SHOOTER_LEFT = 9;
    public static final int SHOOTER_RIGHT = 10;
    // Shooter motor controllers (CANBUS - SparkMax)
    public static final int ELEVATION_MOTOR = 11;
    public static final int AZIMUTH_MOTOR = 12;
    public static final int FEEDER_MOTOR = 17;

    // Conveyor motor controller (CANBUS- SparkMax)
    public static final int CONVEYOR_MOTOR = 13;
    
    // Collector motor controllers (CANBUS - SparkMax)
    public static final int INTAKE_COLLECTOR_MOTOR = 16;

    // Climber Duty Cycle Encoders (these are dios)
    public static final int LEFT_CLIMBER_ABSOLUTE_ENCODER = 0;
    public static final int RIGHT_CLIMBER_ABSOLUTE_ENCODER = 1;

    // Climber motor controllers (CANBUS - TalonFX) 
    public static final int LEFT_CLIMBER_MOTOR = 14;
    public static final int RIGHT_CLIMBER_MOTOR = 15;

    // Pneumatics for climber brake
    public static final int BRAKE_OUT = 2;
    public static final int BRAKE_IN = 3;
    
    // DIO devices
    // These will now be implemented using the integrated motor encoders, probably
    // public static final int PITCH_ENCODER_CHANNEL_A = 1;
    // public static final int PITCH_ENCODER_CHANNEL_B = 2;
    public static final int AZIMUTH_LEFT_LIMIT = 4;
    public static final int AZIMUTH_RIGHT_LIMIT = 5;
    public static final int ELEVATION_UP_LIMIT = 6;
    public static final int ELEVATION_DOWN_LIMIT = 7;

    // Beam Brake Sensor
    public static final int UPPER_BEAM_BREAK_SENSOR = 3;
    public static final int LOWER_BEAM_BREAK_SENSOR = 2;

    // Limit Switches
    public static final int COLLECTOR_IN_LIMIT = 9;
    public static final int COLLECTOR_OUT_LIMIT = 8;

    // LEDs
    public static final SPI.Port SPILEDPort = SPI.Port.kOnboardCS0;
    public static final int nLEDs = 20; // inferred from 2020 robot program

    public static final int PWMLEDPort = 9;
    public static final int nPWMLEDs = 32;
}
