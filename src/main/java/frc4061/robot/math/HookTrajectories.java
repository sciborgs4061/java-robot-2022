package frc4061.robot.math;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import edu.wpi.first.math.MathSharedStore;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Transform2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.spline.PoseWithCurvature;
import edu.wpi.first.math.spline.Spline;
import edu.wpi.first.math.spline.SplineParameterizer.MalformedSplineException;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.trajectory.TrajectoryConfig;
import edu.wpi.first.math.trajectory.TrajectoryGenerator;
import edu.wpi.first.math.trajectory.TrajectoryParameterizer;

public class HookTrajectories {
    private static final Trajectory kDoNothingTrajectory =
      new Trajectory(Arrays.asList(new Trajectory.State()));
    final static Transform2d flip = new Transform2d(new Translation2d(), Rotation2d.fromDegrees(180.0));
    // the splines passed to trajectoryFromSplines should be generated assuming that the robot is
    // going to drive the path going forward. The initial pose should have the robot pointing in the
    // direction of travel (when generating the splines).
    // If config.isReversed(), trajectoryFromSplines will post-process the generated trajectory
    // so that is appropriate for a robot that starts facing the opposite direction and drives backwards.
    public static Trajectory trajectoryFromSplines(Spline[] splines, TrajectoryConfig config) {
      List<PoseWithCurvature> points;
      try {
        points = TrajectoryGenerator.splinePointsFromSplines(splines);
      } catch (MalformedSplineException ex) {
        reportError(ex.getMessage(), ex.getStackTrace());
        return kDoNothingTrajectory;
      }

      Trajectory candidate = TrajectoryParameterizer.timeParameterizeTrajectory(
            points, 
            config.getConstraints(), 
            config.getStartVelocity(),
            config.getEndVelocity(),
            config.getMaxVelocity(), 
            config.getMaxAcceleration(),
            false  // I don't understand all the interactions between spline generation and 
                   // the effect of config.isReversed() in the wpilib implementation.
                   // Thus the edict to only generate trajectories for forward travel. 
                   // In the next step we'll adapt the trajectory for reverse travel ourselves.
            );

      return config.isReversed() ? reverseTrajectory(candidate): candidate;
    }

    // Reversing a trajectory entails
    //    Keeping the points and times the same
    //    Inverting (negating) the velocities, accelerations, and curvatures
    //    Inverting (adding 180 deg, not negating!) the headings
    
    public static Trajectory reverseTrajectory(Trajectory t) {
        List<Trajectory.State> states = t.getStates();
        ArrayList<Trajectory.State> newStates = new ArrayList<Trajectory.State>(states.size());
        Rotation2d halfTurn = Rotation2d.fromDegrees(180);
        for (var s : states) {
          Trajectory.State newState = new Trajectory.State(
            s.timeSeconds,
            -s.velocityMetersPerSecond,
            -s.accelerationMetersPerSecondSq,
            new Pose2d (s.poseMeters.getTranslation(), s.poseMeters.getRotation().plus(halfTurn)),
            -s.curvatureRadPerMeter
          );
          newStates.add(newState);
        }
        return new Trajectory(newStates);
    }
    
    private static void reportError(String error, StackTraceElement[] stackTrace) {
          MathSharedStore.reportError(error, stackTrace);
    }
}

