/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc4061.robot;


import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandGroupBase;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc4061.robot.generated.GitHash;
import frc4061.robot.utilities.SmartDashboardUtil;

/**
 * The VM is configured to automatically run this class, and to call the functions corresponding to
 * each mode, as described in the TimedRobot documentation. If you change the name of this class or
 * the package after creating this project, you must also update the build.gradle file in the
 * project.
 */
public class Robot extends TimedRobot {
  private static final String robotStateKey = "Robot/State";
  private Command m_autonomousCommand;

  private RobotContainer m_robotContainer; 
    
  /**
   * This function is run when the robot is first started up and should be used for any
   * initialization code.
   */
  @Override
  public void robotInit() {
    // Instantiate our RobotContainer.  This will perform all our button bindings, and put our
    // autonomous chooser on the dashboard.
    SmartDashboard.putString("Robot/version", GitHash.gitHash);
    m_robotContainer = RobotContainer.getInstance();
    SmartDashboardUtil.ensureNumber("Autonomous Parameters/Auto Delay", 0);
    SmartDashboardUtil.ensureNumber("Autonomous Parameters/Auto first shot delay", 0);
  }

  /**
   * This function is called every robot packet, no matter the mode. Use this for items like
   * diagnostics that you want to run during disabled, autonomous, teleoperated and test.
   *
   * <p>This runs after the mode specific periodic functions, but before
   * LiveWindow and SmartDashboard integrated updating.
   */
  @Override
  public void robotPeriodic() {
    // Runs the Scheduler.  
    // This is responsible for polling buttons, adding newly-scheduled
    // commands, running already-scheduled commands, removing finished or interrupted commands,
    // and running subsystem periodic() methods. This must be called from the robot's periodic
    // block in order for anything in the Command-based framework to work.
    DriveteamInterface.update();
    CommandScheduler.getInstance().run();
  }

 
  /**
   * This function is called once for simulation
   */
  @Override
  public void simulationInit() {
    SmartDashboard.putBoolean("Driverstation/Drivebase Override", true);
  }
  /**
   * This function is called once each time the robot enters Disabled mode.
   */
  @Override
  public void disabledInit() {
    SmartDashboard.putString(robotStateKey, "Disabled");
  }

  @Override
  public void disabledPeriodic() {}

  @Override
  public void disabledExit() {
    LiveWindow.disableAllTelemetry();
    // Unfortunately, after disabling all telemetry it is not easy to turn
    // all telemtry back on -- it must be done component by component.
    // This means, once the robot has been enabled you can't look at things
    // in livewindow any more until the robot is restarted.
  }

  /**
   * This autonomous runs the autonomous command selected by your {@link RobotContainer} class.
   */
  @Override
  public void autonomousInit() {
    SmartDashboard.putString(robotStateKey, "Autonomous");
    m_autonomousCommand = m_robotContainer.getAutonomousCommand();

    // schedule the autonomous command (example)
    if (m_autonomousCommand != null) {
      CommandGroupBase.clearGroupedCommand(m_autonomousCommand);
      (new WaitCommand(SmartDashboard.getNumber("Autonomous Parameters/Auto Delay", 0))
          .andThen(m_robotContainer.getAutonomousCommand())).schedule();
    }
  }

  /**
   * This function is called periodically during autonomous.
   */
  @Override
  public void autonomousPeriodic() {}

  @Override
  public void teleopInit() {
    // This makes sure that the autonomous stops running when
    // teleop starts running. If you want the autonomous to
    // continue until interrupted by another command, remove
    // this line or comment it out.
    SmartDashboard.putString(robotStateKey, "Teleop");
    if (m_autonomousCommand != null) {
      m_autonomousCommand.cancel();
    }
  }

  /**
   * This function is called periodically during operator control.
   */
  @Override
  public void teleopPeriodic() {}

  @Override
  public void testInit() {
    SmartDashboard.putString(robotStateKey, "Test");
    // Cancels all running commands at the start of test mode.
    CommandScheduler.getInstance().cancelAll();
  }

  /**
   * This function is called periodically during test mode.
   */
  @Override
  public void testPeriodic() {}
}
