package frc4061.robot.Components;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class EnableSwitchWithOverride {
    final String m_dashboardKey;
    final int m_switchId;
    final String m_enabledKey;
    final String m_overrideKey;
    final Joystick m_switchbox;
    boolean m_enabled = false;
    public EnableSwitchWithOverride(String dashboardKey, Joystick switchbox, int switchId, boolean defaultValue, boolean overrideDefault) {
        m_dashboardKey = dashboardKey;
        m_switchbox = switchbox;
        m_switchId = switchId;
        m_enabledKey = m_dashboardKey + " Enabled switch";
        m_overrideKey = m_dashboardKey + " Override";
        m_enabled = defaultValue;
        SmartDashboard.putBoolean(m_overrideKey, overrideDefault);
        SmartDashboard.putBoolean(m_enabledKey, defaultValue);
    }
    public boolean getSwitch() { return m_switchbox.getRawButton(m_switchId); }
    public boolean getOverride() { return SmartDashboard.getBoolean(m_overrideKey, false); }
    public boolean getEnabled() { return m_enabled; }
    public void update() { 
        m_enabled = getSwitch() || getOverride(); 
        SmartDashboard.putBoolean(m_enabledKey, getSwitch());
    }
}