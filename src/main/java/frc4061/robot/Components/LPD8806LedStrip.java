package frc4061.robot.Components;

import edu.wpi.first.wpilibj.SPI;
import frc4061.robot.RobotMap;

public class LPD8806LedStrip {  
    public static final void setColor (int pixel, byte r, byte g, byte b) {
        var c = m_ledContents; // just a short local name
        var pos = 3 * pixel;
        c[pos+0] = (byte) (g | 0x80);  // color order for LPD8806 strips is G, R, B
        c[pos+1] = (byte) (r | 0x80);
        c[pos+2] = (byte) (b | 0x80);
    }

    public static final void setColor (byte r, byte g, byte b) {
        for (var i = 0; i < RobotMap.nLEDs; i++) {
            setColor(i, r, g, b);
        }
    }

    public static final void blank() {
        byte zero = (byte) 0;
        setColor(zero, zero, zero);
        }

    public static void pushToLEDs() {
        m_spiMaster.write(m_ledContents, m_totalBytes);
    }    
        
    private static final SPI m_spiMaster = new SPI(RobotMap.SPILEDPort);
    private static final int m_numPixelBytes = 3 * RobotMap.nLEDs;  // R, G, B for each pixel
    private static final int m_numLatchBytes = (RobotMap.nLEDs + 31) >> 5; // One latch byte per 32 pixels, rounded up
    private static final int m_totalBytes = m_numPixelBytes + m_numLatchBytes;
    private static final byte[] m_ledContents = new byte[m_totalBytes];
    static {
        m_spiMaster.setClockRate(1000000);
        m_spiMaster.setMSBFirst();
        m_spiMaster.setSampleDataOnLeadingEdge();
        m_spiMaster.setClockActiveHigh();
        m_spiMaster.setChipSelectActiveLow();
    }
}
