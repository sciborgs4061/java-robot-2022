// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc4061.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.subsystems.Climber;

public class RunClimber extends CommandBase {
  private Climber m_climber;
  private double m_speed;

  /** Creates a new RunClimber. */
  public RunClimber(Climber climber, double speed) {
    m_climber = climber; 
    m_speed = speed;
    addRequirements(climber);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    
  }

  // Called every time the scheduler runs while the command is scheduled.
  // TODO: find out if these constants are appropriate for adjusting speeds
  // in high-load situations -- in conjunction with looking at the strategy
  // in Climber.setMotorSpeeds
  @Override
  public void execute() {
    final double maxPlay = 0.05;
    final double minPlay = -0.05;
    final double speedUpValue = 0.05;
    final double slowDownValue = 0.05;
    if (m_speed != 0) {
        m_climber.setMotorSpeed(m_speed, maxPlay, minPlay, speedUpValue, slowDownValue);
    } else {
        m_climber.stop();
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    m_climber.stop();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
