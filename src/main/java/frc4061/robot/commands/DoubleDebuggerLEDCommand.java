package frc4061.robot.commands;
import java.util.function.BooleanSupplier;
import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.subsystems.LEDSubset;

public class DoubleDebuggerLEDCommand extends CommandBase {
    LEDSubset m_leds;
    Color m_onBitColor;
    Color m_offBitColor;
    DoubleSupplier[] m_doubleFunctions;
    
    public DoubleDebuggerLEDCommand (LEDSubset leds, DoubleSupplier[] doubles) {
        m_leds = leds;
        m_onBitColor = Color.kGreen;
        m_offBitColor = Color.kRed;
        addRequirements(leds);
        m_doubleFunctions = doubles;
    }

    @Override
    public void execute () {
        // TODO: Going to be assuming that we have enough space for now, should actually verify
        int ledsPerDouble =  m_leds.getLength() / m_doubleFunctions.length;

        for(int i = 0; i < m_doubleFunctions.length; i++){
            int start = i * ledsPerDouble;
            int stop = start + ledsPerDouble;
            setDoubleLeds(m_doubleFunctions[i].getAsDouble(), start, stop);
        }
    }

    

    public boolean isFinished() { return false; }
    public boolean runsWhenDisabled() { return true; }

    private void setDoubleLeds(double value, int start, int stop){
        // assume value is [0..1];
        double range = Math.pow(2,stop-start);
        long  ans = (long) Math.floor(value*range);
        for (int i = 0; i < stop-start; i++) {
            long mask = 1 << i;
            boolean work = ((ans & mask) == mask);
            if (work) {
                m_leds.setLED(i+start, m_onBitColor); 
            } else {
                m_leds.setLED(i+start, m_offBitColor); 
            }
        }
    }
}