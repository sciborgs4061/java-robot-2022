package frc4061.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.subsystems.Collector;

public class  CollectorToggleExtended extends CommandBase {
    private Collector m_collector;

    public CollectorToggleExtended(Collector intake) {
        m_collector = intake;
        addRequirements(intake);
    }

    @Override
    public void initialize() {
        m_collector.setExtended(!m_collector.isExtended());
    }

    @Override
    public boolean isFinished() {
        return true;
    }

    @Override
    public boolean runsWhenDisabled() {
        return false;
    }
}

