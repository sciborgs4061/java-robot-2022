package frc4061.robot.commands;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.spline.LinearPseudoSpline;
import edu.wpi.first.math.spline.Spline;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.Constants;
import frc4061.robot.math.HookTrajectories;
import frc4061.robot.subsystems.Drivebase;
import frc4061.robot.utilities.InchUtils;
import frc4061.robot.utilities.SmartDashboardUtil;

// A command to drive a fixed distance forward (+) or back (-)
// The distance to drive is determined at the time the command is constructed.

public class DriveFixedDistance extends CommandBase {
 
    private Drivebase m_drivebase;
    private FollowTrajectoryFactory m_followerFactory;

    public DriveFixedDistance(Drivebase drivebase, FollowTrajectoryFactory factory, double distance) {
        m_drivebase = drivebase;
        m_followerFactory = factory;
        Command pgm = driveFromTo(70, 70+distance, distance<0);
    }

    private static final Pose2d inchPose(double inchX) {
        return InchUtils.pose2d( inchX, 90, 0);
    }
    
    private final Command driveFromTo(double startLine, double finishLine, boolean reversed) {
        Pose2d startPose = inchPose(startLine);
        Pose2d finishPose = inchPose(finishLine);
        Spline line = new LinearPseudoSpline(startPose.getTranslation(), finishPose.getTranslation());
        Trajectory traj = HookTrajectories.trajectoryFromSplines(new Spline[] {line},
                                                                reversed ? Constants.Drivebase.configReversed : Constants.Drivebase.configForward);
        Command cmd = m_followerFactory.getFollower(traj);
        Command ret = cmd.beforeStarting(() -> m_drivebase.resetOdometry(inchPose(startLine)));
        return ret;
    }

}
