package frc4061.robot.commands;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.subsystems.LEDSubset;

public class DebuggerNetworkTableLEDCommand extends CommandBase {
    LEDSubset m_leds;
    Color m_onBitColor;
    Color m_offBitColor;
    NetworkTableInstance m_networkTable;
    NetworkTable m_table;
    String m_keyName; 
   

    
    public DebuggerNetworkTableLEDCommand (LEDSubset leds, String tableName, String keyName) {
        m_keyName = keyName;
        m_leds = leds;
        m_onBitColor = Color.kGreen;
        m_offBitColor = Color.kRed;
        m_networkTable = NetworkTableInstance.getDefault();
        m_table = m_networkTable.getTable(tableName);
        addRequirements(leds);
    }
    @Override
    public void execute () {
        double variableName = m_table.getEntry(m_keyName).getDouble(-1);
        
        long ans = Double.doubleToRawLongBits(variableName);
        for (int i = 0; i < m_leds.getLength(); i++) {
            long mask = 1 << i;
            boolean work = ((ans & mask) == mask);
            if (work) {
                m_leds.setLED(i, m_onBitColor); 
            } else {
                m_leds.setLED(i, m_offBitColor); 
            }
        }

    }

    

    public boolean isFinished() { return false;}
    public boolean runsWhenDisabled() { return true;}
}