package frc4061.robot.commands;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.Constants;
import frc4061.robot.subsystems.Cameras;
import frc4061.robot.subsystems.Turret;

// TODO: run auto always?
public class AimShooter extends CommandBase {
    private final Turret m_turret;
    private final RunShooter m_runShooter;
    private final NetworkTable m_limelight;

    // TODO: tune down tolerance (maybe it can be more accurate)
    private final double azTolerance = 2.0;
    private final double elevTolerance = 2; // elev has much higher gear ratio
    private final double azTurnMultiplier = -0.2;
    private final double elTurnMultiplier = 1.5;

    public AimShooter(Turret turret, RunShooter runShooter) {
        m_turret = turret;
        m_runShooter = runShooter;
        addRequirements(turret);

        m_limelight = NetworkTableInstance.getDefault().getTable("limelight");
    }

    @Override
    public void execute() {
        // Only try to aim if the limelight has a target
        if (Cameras.getTarget() != 0.0) {
            // adjust azimuth
            var azError = m_limelight.getEntry("tx").getDouble(0);
            if (Math.abs(azError) > azTolerance) {
                m_turret.rotate(azError * azTurnMultiplier);
            } else {
                m_turret.rotate(0);
            }

            // adjust elevation
            var ty = m_limelight.getEntry("ty").getDouble(0);
            var elevEncoder = m_turret.elevation();
            var angle = combinedAngle(ty, elevEncoder);
            var upperEntry = findUpperEntry(angle);
            var targetElevation = getInterpolatedElevation(angle, upperEntry);
            var elevError = targetElevation - elevEncoder;
            if (Math.abs(elevError) > elevTolerance) {
                m_turret.elevate(elevError * elTurnMultiplier);
            } else {
                m_turret.elevate(0);
            }

            // Change the shooter RPM setting (note this does not 
            // actually run the motors)
            var targetVoltage = getInterpolatedVoltage(angle, upperEntry);
            m_runShooter.setShooterVoltageSetpoint(targetVoltage);
        }
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public void end(boolean interrupted) {
        m_turret.rotate(0);
        m_turret.elevate(0);
    }

    @Override
    public boolean runsWhenDisabled() {
        return false;
    }

    double combinedAngle(double ty, double elevEncoder) {
        return ty + (elevEncoder/Constants.Turret.elevationTicksPerDegree);
    }

    // return the index of the first row greater than the 
    // combined angle, but no less than 1 and no greater than
    // aimingData.length-1;
    private int findUpperEntry(double angle) {
        for (int i = 1; i < aimingData.length; i++) {
            if (angle < aimingData[i][0]) return i;
        }
        return aimingData.length-1;
    }

    double getInterpolatedVoltage(double angle, int upperEntry) {
        return getInterpolated(angle, upperEntry, 3);
    }

    double getInterpolatedElevation(double angle, int upperEntry) {
        double candidate = getInterpolated(angle, upperEntry, 3);
        return Math.max(0, candidate);
    }

    double getInterpolated(double angle, int upperEntry, int column) {
        int lowerEntry = upperEntry - 1;
        double upperAngle = aimingData[upperEntry][0];
        double lowerAngle = aimingData[lowerEntry][0];
        double upperValue = aimingData[upperEntry][column];
        double lowerValue = aimingData[lowerEntry][column];
        double angleDiff = upperAngle - lowerAngle;
        double valueDiff = upperValue - lowerValue;
        // now linearly interpolate
        return 
            valueDiff*((angle-lowerAngle)/angleDiff)+lowerValue;
        
    }

    private static double[][] aimingData = {
        // Use either the RPM or the voltage -- voltage is probably better
        // {totalAngle, aimingElevationEncoder, RPM, voltage}
        {-9.8,  0.0, 2050, 7.4},
        {-8.11, 8.54, 0, 7.42},
        { 0.5,  9.1, 1808, 6.6},
        { 2.7,  13.9, 0,   6.6},
        { 6.9, 21.4, 1815, 6.6},  // this row
        {10.3, 26.9, 0,    6.6},
        {12.64, 19.0, 1715, 6.27},
        {13.14, 21.3, 0,   6.31},
        {21.4, 34.0, 1765, 6.43}  // and this row are suspect
    };
}
