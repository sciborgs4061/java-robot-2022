package frc4061.robot.commands;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.subsystems.Turret;

// TODO: run auto always?
public class AlignTurret extends CommandBase {
    private final Turret m_turret;
    private final NetworkTable m_limelight;

    // TODO: tune down tolerance (maybe it can be more accurate)
    private final double tolerance = 2;
    private final double turnMultiplier = -0.1;

    private double error = 0;

    public AlignTurret(Turret turret) {
        m_turret = turret;
        addRequirements(turret);

        m_limelight = NetworkTableInstance.getDefault().getTable("limelight");
    }

    @Override
    public void execute() {
        error = m_limelight.getEntry("tx").getDouble(0);
        m_turret.rotate(error * turnMultiplier);

        // TODO: calculate Y error and trajectory (not a straight line)
    }

    @Override
    public boolean isFinished() {
        return Math.abs(error) < tolerance;
    }

    @Override
    public void end(boolean interrupted) {
        m_turret.rotate(0);
    }

    @Override
    public boolean runsWhenDisabled() {
        return false;
    }
}
