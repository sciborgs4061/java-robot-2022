package frc4061.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.Constants;
import frc4061.robot.subsystems.Climber;

public class ClimberHome extends CommandBase {

    private Climber m_climber;
    private double m_startPos = Constants.ClimberConstants.startPosition;
    private double m_startTolerance = Constants.ClimberConstants.startTolerance;
    private double m_homingSpeed = Constants.ClimberConstants.homingSpeed;
    private double m_leftError;
    private double m_rightError;
    
    public ClimberHome (Climber climber) {
        m_climber = climber;
        addRequirements(climber);
    }

    @Override
    public void initialize() {

    }

    // We are assuming that arms start near 0 and that 
    // the target starting position is not too far from 0
    @Override
    public void execute() {
        double leftPos = m_climber.getLeftAbsolutePosition();
        double rightPos = m_climber.getRightAbsolutePosition();
        // adjust large positions to be negative and closer to 0
        leftPos = leftPos > 0.5 ? leftPos - 1.0 : leftPos;
        rightPos = rightPos > 0.5 ? rightPos - 1.0 : rightPos;
        m_leftError = m_startPos - leftPos;
        m_rightError = m_startPos - rightPos;
        double leftFraction = Math.abs(m_leftError) < m_startTolerance ? 0.0 : Math.copySign(m_homingSpeed, m_leftError);
        double rightFraction = Math.abs(m_rightError) < m_startTolerance ? 0.0 : Math.copySign(m_homingSpeed, m_rightError);
        m_climber.setMotorFractions(leftFraction, rightFraction); 
    }

    @Override
    public void end(boolean interrupted) {
        m_climber.stop();
        // Henceforth angles are measured from the starting position
        m_climber.resetEncoders();
    }

    @Override
    public boolean isFinished() {
        return (Math.abs(m_leftError) < m_startTolerance) && (Math.abs(m_rightError) < m_startTolerance);
    }

    @Override
    public boolean runsWhenDisabled() {
        return false;
    }
    
}
