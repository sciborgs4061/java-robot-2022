package frc4061.robot.commands;
import java.util.function.BooleanSupplier;
import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.subsystems.LEDSubset;

public class BooleanDebuggerLEDCommand extends CommandBase {
    LEDSubset m_leds;
    Color m_onBitColor;
    Color m_offBitColor;
    BooleanSupplier[] m_booleanFunctions;
    
    public BooleanDebuggerLEDCommand (LEDSubset leds, BooleanSupplier[] bools) {
        m_leds = leds;
        m_onBitColor = Color.kGreen;
        m_offBitColor = Color.kPurple;
        addRequirements(leds);
        m_booleanFunctions = bools;
    }

    @Override
    public void execute () {
        // TODO: Going to be assuming that we have enough space for now, should actually verify
        for (int i = 0; i < m_booleanFunctions.length; i++) {
            var boolFunc = m_booleanFunctions[i];
            boolean value = boolFunc.getAsBoolean();
            var color = value ? m_onBitColor : m_offBitColor;
            m_leds.setLED(i, color);
        }
        // turn off any remaining LEDs to avoid confusion
        for (int i = m_booleanFunctions.length; i<m_leds.getLength(); i++) {
            m_leds.setLED(i, Color.kBlack);
        }
    }

    

    public boolean isFinished() { return false; }
    public boolean runsWhenDisabled() { return true; }
}