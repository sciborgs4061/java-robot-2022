package frc4061.robot.commands;

import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.trajectory.TrajectoryUtil;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj2.command.Command;

import java.io.IOException;
import java.nio.file.Path;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.RamseteController;
import edu.wpi.first.wpilibj2.command.RamseteCommand;
import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import frc4061.robot.subsystems.Drivebase;
import frc4061.robot.Constants;

public class FollowTrajectoryFactory {

    private final Drivebase m_drivebase;

    public FollowTrajectoryFactory(Drivebase drivebase) {
        m_drivebase = drivebase;
        // other stuff common to all trajectory-followers on this drivebase
    }

    public Command getFollowerForPathweaverJson(String trajectoryJSON) {
        Path trajectoryPath = Filesystem.getDeployDirectory().toPath().resolve("paths/" + trajectoryJSON);
        try {     
           var trajectory = TrajectoryUtil.fromPathweaverJson(trajectoryPath);
           return getFollower(trajectory);
        } catch (IOException ex) {
            DriverStation.reportError("Unable to open trajectory: " + trajectoryPath, ex.getStackTrace());
            return null; // FIX THIS - return an LED command to flash red or some other alarm indicator
        }
    }
    
    public Command getFollower (Trajectory trajectory) {
        RamseteCommand ramseteCommand = new RamseteCommand (
            trajectory,
            m_drivebase.m_odometry::getPoseMeters,
            new RamseteController(Constants.Drivebase.kRamseteB, Constants.Drivebase.kRamseteZeta),
            new SimpleMotorFeedforward(Constants.Drivebase.ksVolts,
                                       Constants.Drivebase.kvVoltSecondsPerMeter,
                                       Constants.Drivebase.kaVoltSecondsSquaredPerMeter),
            Constants.Drivebase.kDriveKinematics,
            m_drivebase::getWheelSpeeds,
            new PIDController(Constants.Drivebase.kPDriveVel, 0, 0),
            new PIDController(Constants.Drivebase.kPDriveVel, 0, 0),
            m_drivebase::tankDriveVolts,
            m_drivebase
        );

        ramseteCommand.addRequirements(m_drivebase);
        return ramseteCommand.beforeStarting(() -> 
                   m_drivebase.resetOdometry(trajectory.getInitialPose()));
    }
}
