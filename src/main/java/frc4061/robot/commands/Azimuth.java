package frc4061.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.subsystems.Turret;

public class Azimuth extends CommandBase {
    private final Turret m_turret;
    private final double m_speed;

    public Azimuth(Turret turret, double speed) {
        m_turret = turret;
        m_speed = speed;
        addRequirements(turret);
    }

    @Override
    public void execute() {
        m_turret.rotate(m_speed);
    }

    @Override
    public void end(boolean interrupted) {
        m_turret.rotate(0);
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public boolean runsWhenDisabled() {
        return false;
    }
}