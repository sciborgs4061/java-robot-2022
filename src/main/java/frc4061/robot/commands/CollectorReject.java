package frc4061.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.Constants;
import frc4061.robot.subsystems.Collector;

public class CollectorReject extends CommandBase {
    private final Collector m_collector;

    public CollectorReject(Collector collector) {
        m_collector = collector;
        addRequirements(collector);
    }

    @Override
    public void execute() {
        m_collector.reject(Constants.Collector.rejectSpeed);
    }

    @Override
    public void end(boolean interrupted) {
        m_collector.stop();
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public boolean runsWhenDisabled() {
        return false;
    }
}
