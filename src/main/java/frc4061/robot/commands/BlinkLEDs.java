package frc4061.robot.commands;

import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.subsystems.LEDSubset;
//blinks way to fast
public class BlinkLEDs extends CommandBase {
    LEDSubset m_leds;
    Color m_color;
    boolean m_blink = true;
    int m_count = 0;
    
    public BlinkLEDs (LEDSubset leds, Color color) {
        m_leds = leds;
        m_color = color;
        addRequirements(leds);
    }
    @Override
    public void execute () {
        m_count++;
        if (m_count > 1000) {
            m_count = 0;
        }
        if (m_count % 20 == 0) {
            if (m_blink == false) {
                for (int i=0; i<m_leds.getLength(); i++) {
                    m_leds.setLEDOff(i);
                }
                m_blink = true; 
            } else {
                for (int i=0; i<m_leds.getLength(); i++) {
                    m_leds.setLED(i, m_color);
                }
                m_blink = false; 
            }
        }
    }

    public boolean isFinished() { return false;}
    public boolean runsWhenDisabled() { return true;}
}
