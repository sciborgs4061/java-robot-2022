// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc4061.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.subsystems.Shooter;

public class ToggleFlyWheel extends CommandBase {
  private final Shooter m_shooter;

  public ToggleFlyWheel(Shooter shooter) {
    m_shooter = shooter;
  }

  @Override
  public void initialize() {}

  @Override
  public void execute() {
    m_shooter.setFlywheelOnOff(true);
  }

  @Override
  public void end(boolean interrupted) {
    m_shooter.setFlywheelOnOff(false);
  }

  @Override
  public boolean isFinished() {
    return false;
  }
}
