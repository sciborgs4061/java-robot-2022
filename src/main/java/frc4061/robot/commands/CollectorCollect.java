package frc4061.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.Constants;
import frc4061.robot.subsystems.Collector;

public class CollectorCollect extends CommandBase {
    private Collector m_collector;

    public CollectorCollect(Collector intake) {
        m_collector = intake;
        addRequirements(intake);
    }

    @Override
    public void execute() {
        m_collector.collect(Constants.Collector.collectSpeed);
    }

    @Override
    public void end(boolean interrupted) {
        m_collector.stop();
        m_collector.retract();
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public boolean runsWhenDisabled() {
        return false;
    }
}
