package frc4061.robot.commands;

import java.util.Set;
import java.util.function.BooleanSupplier;

import edu.wpi.first.wpilibj.RobotState;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.ProxyScheduleCommand;

public class BooleanEvent extends ProxyScheduleCommand {

    private BooleanSupplier m_supplier;
    private Set<Command> m_toSchedule;
    private boolean m_runsWhileDisabled;

    public BooleanEvent(BooleanSupplier supplier, boolean runsCommandsWhileDisabled, Command... toSchedule){
        m_supplier = supplier;
        m_toSchedule = Set.of(toSchedule);
        m_runsWhileDisabled = runsCommandsWhileDisabled;
    }

    @Override
    public void initialize(){
        // Do nothing
    }

    @Override
    public void execute(){
        if(m_runsWhileDisabled || !RobotState.isDisabled())
        {
            for(var command : m_toSchedule)
            {
                if(m_supplier.getAsBoolean())
                {
                    command.schedule();
                }
                else if(command.isScheduled())
                {
                    command.cancel();
                }
            }
        }
    }

    @Override
    public boolean isFinished()
    {
        return false;
    }

    @Override
    public boolean runsWhenDisabled(){
        return true;
    }
}
