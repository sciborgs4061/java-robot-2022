package frc4061.robot.commands;

import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.subsystems.LEDSubset;

public class SolidColorLEDs extends CommandBase {
    LEDSubset m_leds;
    Color m_color;
    
    public SolidColorLEDs (LEDSubset leds, Color color) {
        m_leds = leds;
        m_color = color;
        addRequirements(leds);
    }
    @Override
    public void initialize () {
        for (int i=0; i<m_leds.getLength(); i++) {
            m_leds.setLED(i, m_color);
        }
    }

    public boolean isFinished() { return false;}
    public boolean runsWhenDisabled() { return true;}
}
