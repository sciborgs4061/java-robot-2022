package frc4061.robot.commands;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.Constants.ShooterConstants;
import frc4061.robot.Constants;
import frc4061.robot.DriveteamInterface;
import frc4061.robot.subsystems.Shooter;
import frc4061.robot.utilities.SmartDashboardUtil;

public class RunShooter extends CommandBase {
    private final String d_flywheelRPMSetpoint = "Shooter/Flywheel RPM Setpoint";
    private final String d_flywheelVoltageSetpoint = "Shooter/Flywheel Voltage Setpoint";
    private final Shooter m_shooter;

    public RunShooter(Shooter shooter) {
        m_shooter = shooter;
        addRequirements(shooter);
        SmartDashboardUtil.ensureNumber(d_flywheelRPMSetpoint, ShooterConstants.kInitialRPMSetpoint);
        SmartDashboardUtil.ensureNumber(d_flywheelVoltageSetpoint, ShooterConstants.kInitialVoltageSetpoint);
    }

    @Override
    public void execute() {
        double flywheelRPM = SmartDashboard.getNumber(d_flywheelRPMSetpoint, 0);
        if (!DriveteamInterface.shooterEnabled()) {
            m_shooter.stopShooter();
            return;
        }
        m_shooter.setRPM(flywheelRPM);
        if (flywheelRPM==0.0) {
            double flywheelVoltage = SmartDashboard.getNumber(d_flywheelVoltageSetpoint, 0);
            m_shooter.setOutput(flywheelVoltage);
        }
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public void end(boolean interrupted) {
        m_shooter.stopShooter();
    }

    @Override
    public boolean runsWhenDisabled() {
        return false;
    }

    public void setShooterVoltageSetpoint(double voltage) {
        SmartDashboard.putNumber(d_flywheelRPMSetpoint, 0.0);
        SmartDashboard.putNumber(d_flywheelVoltageSetpoint, voltage);
    }

    public void adjustShooterRPMsetpoint(double deltaRPM) {
        // This works as it does so that it doesn't change the behavior of the shooter
        // control that if the RPM is set it takes precedence, but if the RPM is 0
        // the voltage can be set. Regardless of which is in use, it is intended that
        // the command change the setpoint by about 50 RPM
        double rpmSetpoint = SmartDashboard.getNumber(d_flywheelRPMSetpoint, 0.0);
        if (rpmSetpoint != 0.0) {
            SmartDashboard.putNumber(d_flywheelRPMSetpoint, rpmSetpoint+deltaRPM);
        } else {
            double voltageSetpoint = SmartDashboard.getNumber(d_flywheelVoltageSetpoint, 0.0);
            SmartDashboard.putNumber(d_flywheelVoltageSetpoint, voltageSetpoint+deltaRPM*Constants.ShooterConstants.kVoltPerRPM);
        }
    }
}
