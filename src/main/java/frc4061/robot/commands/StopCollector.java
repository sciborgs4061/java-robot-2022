package frc4061.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.subsystems.Collector;

public class StopCollector extends CommandBase {
    private final Collector m_collector;

    public StopCollector(Collector collector) {
        m_collector = collector;
        addRequirements(collector);
    }

    @Override
    public void initialize() {
        m_collector.stop();
    }

    @Override
    public boolean runsWhenDisabled() {
        return false;
    }
}
