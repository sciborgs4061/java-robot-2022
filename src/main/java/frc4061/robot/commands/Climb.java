package frc4061.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.DriveteamInterface;
import frc4061.robot.subsystems.Climber;

public class Climb extends CommandBase {

    private Climber m_climber;
    
    public Climb (Climber climber) {
        m_climber = climber;
        addRequirements(climber);
    }
// the rest of the code is not done at all
// Assumes that the climber encoders are set to 0
// in starting position (SP)
// Manually latch on (drive backward from SP)
// At that point the encoders will read -0.575
// Drive Forward to -0.131 -- hooks past next bar
// Drive Backward to -0.172 
// Continue Backard to -0.380 -- past the traverse?
// Forward to +0.366 -- past the traverse?
// Back to +0.141 -- latch onto traverse
// 

    @Override
    public void initialize() {

    }

    // We are assuming that arms start near 0 and that 
    // the target starting position is not too far from 0
    @Override
    public void execute() {
        double root2 = Math.sqrt(2);

        double leftJoystick = DriveteamInterface.Operator.getClimberSpeed() / root2;
        double rightJoystick = DriveteamInterface.Operator.getRightOnlyClimberSpeed() / root2;
        // inhibit using both sticks at once
        // use squared-inputs to give finer control at lower powers
        // TODO: Assess whether squared inputs for climber joysticks is desirable
        // TODO: Assess size of deadband
        double leftFraction = Math.abs(leftJoystick) > 0.1 ? Math.copySign(leftJoystick*leftJoystick, leftJoystick) : 0.0;
        double rightFraction = Math.abs(rightJoystick) > 0.1 ? Math.copySign(rightJoystick*rightJoystick, rightJoystick) : 0.0;
        if (leftFraction != 0.0) {
            m_climber.setMotorFractions(leftFraction, leftFraction);
        } else {
            m_climber.setMotorFractions(0, rightFraction);
        }
     
    }

    @Override
    public void end(boolean interrupted) {
        m_climber.stop();
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public boolean runsWhenDisabled() {
        return false;
    }
    
}
