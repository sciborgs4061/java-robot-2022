package frc4061.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.subsystems.Turret;

public class HomeTurret extends CommandBase {
    private final Turret m_turret;

    public HomeTurret(Turret turret) {
        m_turret = turret;
        addRequirements(turret);
    }

    @Override
    public void execute() {
        // rotate until limit switches are hit
        if (!m_turret.atLeftLimit()) { 
            m_turret.rotate(-0.5);
        }
        else {
            m_turret.rotate(0);
        }
        if (!m_turret.atLowerLimit()) {
            m_turret.elevate(-0.5);
        } else {
            m_turret.elevate(0);
        }
    }

    @Override
    public void end(boolean interrupted) {
        // when the command ends, reset the encoders to 0
        // all future rotations are relative to this
        m_turret.resetEncoders();
        m_turret.rotate(0);
        m_turret.elevate(0);
    }

    @Override
    public boolean isFinished() {
        // stop adjusting when both limit switches have been hit
        return m_turret.atLimits();
    }
}
