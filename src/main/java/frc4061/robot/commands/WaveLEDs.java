package frc4061.robot.commands;

import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.subsystems.LEDSubset;

public class WaveLEDs extends CommandBase {
    LEDSubset m_leds;
    Color m_color;
    int m_pong = 0;

    
    public WaveLEDs(LEDSubset leds, Color color) {
        m_leds = leds;
        m_color = color;
        addRequirements(leds);
    }
    @Override
    public void execute() {
        m_pong++;
        if (m_pong > m_leds.getLength()) {
            m_pong = 0;
        }
            for (int i=0; i<m_leds.getLength(); i++) {
                if (i < m_pong || (i > m_pong + 4)) {
                    m_leds.setLEDOff(i);
                } else {
                    m_leds.setLED(i, m_color); 
                }
            }
    }

    public boolean isFinished() { return false;}
    public boolean runsWhenDisabled() { return true;}
}
