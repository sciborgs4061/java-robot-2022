package frc4061.robot.commands;


import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj.util.Color;
import frc4061.robot.subsystems.LEDSubset;

public class AllianceColor extends PongColorLEDs {

    public AllianceColor(LEDSubset leds) {
        super(leds, Color.kWhite);
        addRequirements(leds);
    }
    @Override
    public void execute() {
        if (DriverStation.getAlliance() == Alliance.Red) {
            m_color = Color.kRed;
        }
        else if (DriverStation.getAlliance() == Alliance.Blue) {
            m_color = Color.kBlue;
        }
        else {
            m_color = Color.kDarkGray;
        }

        super.execute();
    }
    @Override
    public boolean isFinished() {
        return false;
    }
    @Override
    public boolean runsWhenDisabled() {
        return true;
    }
}
