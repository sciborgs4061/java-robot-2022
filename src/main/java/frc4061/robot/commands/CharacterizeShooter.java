package frc4061.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.subsystems.Shooter;
import frc.SysIdGeneralMechanismLogger;

public class CharacterizeShooter extends CommandBase {
    private final Shooter m_shooter;
    private SysIdGeneralMechanismLogger m_logger;
     
    public CharacterizeShooter(Shooter shooter) {

        m_shooter = shooter;
        addRequirements(m_shooter);
    }

    // Called when the command is initially scheduled.
    @Override
    public void initialize() {
        m_logger = new SysIdGeneralMechanismLogger();
        m_logger.updateThreadPriority();
        m_logger.initLogging();
        m_shooter.setFlywheelOnOff(true);
    }
   
    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        double speed = m_shooter.getRPM();
            m_logger.log(0.0, speed);
            m_shooter.setOutput(m_logger.getMotorVoltage());
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        System.out.println("Shooter characterization done; disabled");
        m_shooter.stopShooter();
        m_logger.sendData();
        m_shooter.setFlywheelOnOff(false);
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public boolean runsWhenDisabled() {
        return false;

    }
}

