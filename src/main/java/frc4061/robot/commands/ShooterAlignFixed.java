// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc4061.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import frc4061.robot.subsystems.Shooter;
import frc4061.robot.subsystems.Turret;

public class ShooterAlignFixed extends ParallelCommandGroup {
  private final Turret m_turret;
  private final Shooter m_shooter;
  private final double m_elevation;
  private final double m_azimuth;
  private final double m_shooterRPM;
  private final SetElevation m_elevationSet;
  private final SetRotation m_rotationSet;
  private final Command m_speedSet;

  public ShooterAlignFixed(Turret turret, Shooter shooter, double elevation, double azimuth, double shooterRPM) {
    m_turret = turret;
    m_shooter = shooter;
    m_elevation = elevation;
    m_azimuth = azimuth;
    m_shooterRPM = shooterRPM;
    m_elevationSet = new SetElevation(m_turret, m_elevation);
    m_rotationSet = new SetRotation(m_turret, m_azimuth);
    m_speedSet = new InstantCommand(()-> m_shooter.setRPM(m_shooterRPM));
    addCommands(m_elevationSet, m_rotationSet, m_speedSet);
  }
}