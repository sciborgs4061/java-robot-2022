package frc4061.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.subsystems.Shooter;

public class RunFeeder extends CommandBase {
    private final Shooter m_shooter;

    public RunFeeder(Shooter shooter) {
        m_shooter = shooter;
    }

    @Override
    public void initialize() {
        m_shooter.toggleFeeder(true);
    }

    @Override
    public void end(boolean interrupted) {
        m_shooter.toggleFeeder(false);
    }
}
