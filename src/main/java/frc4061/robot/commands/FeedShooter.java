// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc4061.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.subsystems.Conveyor;

public class FeedShooter extends CommandBase {
  private Conveyor m_conveyor;

  public FeedShooter(Conveyor conveyor) {
    m_conveyor = conveyor;
    addRequirements(conveyor);
  }

  @Override
  public void initialize() {}

  @Override
  public void execute() {
    m_conveyor.setIn();
  }

  @Override
  public void end(boolean interrupted) {
    m_conveyor.stop();
  }

  @Override
  public boolean isFinished() {
    return false;
  }
}
