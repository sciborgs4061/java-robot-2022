package frc4061.robot.commands;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.Constants;
import frc4061.robot.subsystems.Turret;

public class SetElevation extends CommandBase {
    private final Turret m_turret;
    private final double m_target;

    private final PIDController m_pid;

    // TODO: find tolerance
    private final double tolerance = 2.0;

    public SetElevation(Turret turret, double elevation) {
        m_turret = turret;
        m_target = elevation;

        m_pid = new PIDController(Constants.Turret.elevationKP, 0, 0);

        // addRequirements(turret);
    }

    @Override
    public void execute() {
        m_turret.elevate(m_pid.calculate(m_turret.elevation(), m_target));
    }

    @Override
    public boolean isFinished() {
        return Math.abs(m_turret.elevation() - m_target) < tolerance;
    }

    @Override
    public void end(boolean interrupted) {
        m_turret.elevate(0);
    }
}
