package frc4061.robot.commands;

import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.subsystems.LEDSubset;

public class PongColorLEDs extends CommandBase {
    LEDSubset m_leds;
    Color m_color;
    int m_pong = 0;
    boolean m_dir = false;
    
    public PongColorLEDs(LEDSubset leds, Color color) {
        m_leds = leds;
        m_color = color;
        addRequirements(leds);
    }
    @Override
    public void execute() {
        if (m_pong > m_leds.getLength()) {
            m_dir = true;
        }
        if (m_pong < 0) {
            m_dir = false;
        }
        if (m_dir == false) {
            m_pong++;
        } else {
            m_pong--;
        }
            for (int i=0; i<m_leds.getLength(); i++) {
                if (i < m_pong || (i > m_pong + 4)) {
                    m_leds.setLEDOff(i);
                } else {
                    m_leds.setLED(i, m_color); 
                }
            }
    }
    public boolean isFinished() { return false;}
    public boolean runsWhenDisabled() { return true;}
}
