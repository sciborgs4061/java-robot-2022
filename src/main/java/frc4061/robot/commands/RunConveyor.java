// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc4061.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.DriveteamInterface;
import frc4061.robot.subsystems.Conveyor;

public class RunConveyor extends CommandBase {
  private Conveyor m_conveyor;
  private final DisarmShooter m_disarmShooter;


  public RunConveyor(Conveyor conveyor) {
    m_conveyor = conveyor;
    addRequirements(conveyor);
    m_disarmShooter = new DisarmShooter(m_conveyor);
  }

  @Override
  public void initialize() {}

  @Override
  public void execute() {
    //value calculation for the triggers and that control the conveyor (deadband).
    double inSpeed = DriveteamInterface.Operator.operatorXbox.getRightTriggerAxis() - 0.1;
    double outSpeed = DriveteamInterface.Operator.operatorXbox.getLeftTriggerAxis() - 0.1;
    //sets a variable that tells wether the triggers are being pushed past their deadband.
    boolean manualPower = (inSpeed > 0 || outSpeed > 0);

    //this part actually runs the conveyor.
    if (DriveteamInterface.conveyorEnabled()) {
      //figures out if the logic is allowed to run.
      if (DriveteamInterface.conveyorAutoFeedEnabled() && !manualPower) {
        //automatic mode that takes in the ball and stores it using the beam break sensors
        if (m_conveyor.lowerBallPresent() && !m_conveyor.upperBallPresent()) {
          m_conveyor.setIn();
        } else if (m_conveyor.upperBallPresent() && !m_conveyor.lowerBallPresent()) {
          m_disarmShooter.schedule();
        } else {
          m_conveyor.stop();
        }
      // manual mode that is controlled by the switchbox switch 9. figures out if the manual mode is to be run
      } else if (manualPower) {
        if (outSpeed > 0) {
          m_conveyor.setSpeed(-outSpeed * 0.5);
        } else if (inSpeed > 0) {
          m_conveyor.setSpeed(inSpeed * 0.5);
        } else {
          m_conveyor.stop();
        }
      } else {
        m_conveyor.stop();
      } 
    } else {
      m_conveyor.stop();
    }
  }

  @Override
  public void end(boolean interrupted) {
    m_conveyor.stop();
  }

  @Override
  public boolean isFinished() {
    return false;
  }
}