// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc4061.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc4061.robot.DriveteamInterface;
import frc4061.robot.subsystems.Turret;

public class RunTurret extends CommandBase {
  private final Turret m_turret;
  /** Creates a new RunTurret. */
  public RunTurret(Turret turret) {
    m_turret = turret;
    addRequirements(turret);
    
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    //azimuth controls
    m_turret.rotate(DriveteamInterface.Operator.operatorXbox.getRightX() * 0.2);
    m_turret.elevate(-DriveteamInterface.Operator.operatorXbox.getLeftY() * 0.2);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
