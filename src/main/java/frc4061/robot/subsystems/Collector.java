package frc4061.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc4061.robot.Constants;
import frc4061.robot.DriveteamInterface;
import frc4061.robot.RobotMap;

public class Collector extends SubsystemBase {
    private final CANSparkMax m_intakeMotor;
    private final DoubleSolenoid m_collectorSolenoid;
    private double m_extendedAt;
    private boolean m_extended;


    public Collector() {
        m_intakeMotor = getConfiguredMotorController(RobotMap.INTAKE_COLLECTOR_MOTOR);
        m_intakeMotor.setIdleMode(IdleMode.kBrake);
        m_collectorSolenoid = new DoubleSolenoid(PneumaticsModuleType.CTREPCM, RobotMap.COLLECTOR_OUT, RobotMap.COLLECTOR_IN);
    }

    public void periodic() {
        String state;
        if (m_extendedAt == 0) {
            state = "Uninitialized";
        } else {
            state = m_extended ? "Extended" : "Retracted";
        }
        SmartDashboard.putString("Collector/Extension", state);
        if (!DriveteamInterface.collectorEnabled()) {
            stop();
        }
    }

    public void extend() {
        setExtended(true);
    }

    public void retract() {
        setExtended(false);
    }
  
    public void setExtended(boolean extend) {
        if (!DriveteamInterface.collectorEnabled()) return;
        if ((m_extendedAt != 0) && (extend == m_extended)) return; // already in right state
        m_collectorSolenoid.set(
            extend  
                ? DoubleSolenoid.Value.kForward // forward to extend, reverse to retract; interchange if wrong
                : DoubleSolenoid.Value.kReverse);
        m_extended = extend;
        if (m_extended) {
            m_extendedAt = Timer.getFPGATimestamp(); 
        }        
    }

    public boolean isExtended() {
        return m_extended;
    }

    // take absolute value of speed so collect is always inward
    // reject is always outward
    public void collect(double speed) {
        if (!DriveteamInterface.collectorEnabled()) return;
        runAfterExtending(Math.abs(speed));
    }

    public void reject(double speed) {
        if (!DriveteamInterface.collectorEnabled()) return;
        runAfterExtending(-Math.abs(speed));
    }

    public void stop() {
        m_intakeMotor.set(0);
    }

    // private because it must only be called after ensuring Collector subsystem
    // is enabled
    private void runAfterExtending(double speed) {
        extend();
        if ((Timer.getFPGATimestamp() - m_extendedAt) > Constants.Collector.delayAfterExtend) {
            m_intakeMotor.set(speed);
        }
    }

    private static final CANSparkMax getConfiguredMotorController(int CANId) {
        return new CANSparkMax(CANId, MotorType.kBrushless);
    }
    
}
