package frc4061.robot.subsystems;

import edu.wpi.first.wpilibj.AddressableLEDBuffer;
import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj.util.Color8Bit;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class LEDSubset extends SubsystemBase{
    private final int m_first;
    private final int m_length;
    private final int m_lastPlus1;
    AddressableLEDBuffer m_ledBuffer = LEDs.m_ledBuffer;
    public LEDSubset (int first, int lastPlus1, String name) {
        m_first = first;
        m_lastPlus1 = lastPlus1;
        m_length = m_lastPlus1 - m_first;
        m_ledBuffer = LEDs.m_ledBuffer;
        setName(name);
        register();
    }

    public void setHSV (int index, int h, int s, int v) {
        checkBounds(index);
        m_ledBuffer.setHSV(m_first+index, h, s, v);
    }
    public void setRGB(int index, int r, int g, int b) {
        checkBounds(index);
        m_ledBuffer.setRGB(m_first+index, r, g, b);
    }
    public void setLED(int index, Color c) {
        checkBounds(index);
        m_ledBuffer.setLED(m_first+index, c);
    }
    public void setLED(int index, Color8Bit c) {
        checkBounds(index);
        m_ledBuffer.setLED(m_first+index, c);
    }
    public int getLength() {return m_length;}
    public Color getLED(int index) {
        checkBounds(index);
        return m_ledBuffer.getLED(m_first+index);
    }
    public Color8Bit getLEDBit(int index) {
        checkBounds(index);
        return m_ledBuffer.getLED8Bit(m_first+index);
    }
    public void setLEDOff(int index) {
        checkBounds(index);
        m_ledBuffer.setRGB(m_first+index, 0, 0, 0);
    }

    private void checkBounds(int index) {
        if (index >= 0 && index < m_length) return;
        throw new ArrayIndexOutOfBoundsException("Bounds exception in " + getName()); 
    }
}
