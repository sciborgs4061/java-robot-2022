// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc4061.robot.subsystems;


import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DutyCycleEncoder;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc4061.robot.Constants;
import frc4061.robot.DriveteamInterface;
import frc4061.robot.RobotMap;

public class Climber extends SubsystemBase {
    /** Creates a new Climber. */
    private final WPI_TalonFX m_leftMotor;
    private final WPI_TalonFX m_rightMotor;
    private final DutyCycleEncoder m_leftEncoder;
    private final DutyCycleEncoder m_rightEncoder;
    private final DoubleSolenoid m_brakeSolenoid;
    private static final String speedAdjustKey = "Climber/right adjust";
    private static final String leftPositionKey =  "Climber/position left";
    private static final String rightPositionKey =  "Climber/position right";
    private static final String leftMotorCurrentKey = "Climber/motor current left";
    private static final String rightMotorCurrentKey = "Climber/motor current right";
    private static final String leftAbsPositionKey = "Climber/absolute position left";
    private static final String rightAbsPositionKey = "Climber/absolute position right";
    private static final String leftMotorInputKey = "Climber/motor input left";
    private static final String rightMotorInputKey = "Climber/motor input right";

    public Climber() {
        
        m_leftEncoder = new DutyCycleEncoder(RobotMap.LEFT_CLIMBER_ABSOLUTE_ENCODER);
        m_rightEncoder = new DutyCycleEncoder(RobotMap.RIGHT_CLIMBER_ABSOLUTE_ENCODER);
        m_leftMotor = new WPI_TalonFX(RobotMap.LEFT_CLIMBER_MOTOR);
        m_rightMotor = new WPI_TalonFX(RobotMap.RIGHT_CLIMBER_MOTOR);
        m_rightMotor.setInverted(true);
        m_rightMotor.setNeutralMode(NeutralMode.Brake);
        m_leftMotor.setNeutralMode(NeutralMode.Brake);
        m_brakeSolenoid = new DoubleSolenoid(PneumaticsModuleType.CTREPCM, RobotMap.BRAKE_OUT, RobotMap.BRAKE_IN);
    }

    @Override
    public void periodic() {
        double leftPosition = getLeftPosition();
        double rightPosition = getRightPosition();
        SmartDashboard.putNumber(leftPositionKey, leftPosition);
        SmartDashboard.putNumber(rightPositionKey, rightPosition);
        SmartDashboard.putNumber(leftMotorCurrentKey, m_leftMotor.getSupplyCurrent());
        SmartDashboard.putNumber(rightMotorCurrentKey, m_rightMotor.getSupplyCurrent());
        SmartDashboard.putNumber(leftAbsPositionKey, getLeftAbsolutePosition());
        SmartDashboard.putNumber(rightAbsPositionKey, getRightAbsolutePosition());
        SmartDashboard.putNumber(leftMotorInputKey, m_leftMotor.get());
        SmartDashboard.putNumber(rightMotorInputKey, m_rightMotor.get());
        if (!DriveteamInterface.climberEnabled()) {
            stop();
        }
    }

    // TODO: figure out if this adjustment strategy is sufficient for high-load situations
    // such as occur when actually lifting the robot.
    public void setMotorSpeed( double speed, double maxPlay, double minPlay,  double speedUpValue, double slowDownValue) {
        double leftPosition = getLeftPosition();
        double rightPosition = getRightPosition();
        double difference = leftPosition - rightPosition;
        if (DriveteamInterface.climberEnabled()) {
            _releaseBrake();
            m_leftMotor.set(speed);
            if (difference < minPlay) {
                m_rightMotor.set(speed - slowDownValue);
                SmartDashboard.putString(speedAdjustKey, "Slow down");
            } else if (difference > maxPlay) {
                m_rightMotor.set(speed + speedUpValue);
                SmartDashboard.putString(speedAdjustKey, "Speed up");
            } else {
                m_rightMotor.set(speed);
                SmartDashboard.putString(speedAdjustKey, "None");
            }
        } else {
            stop();
        }
    }

    // resetEncoders should (only) be called when the arms
    // are in the starting position.
    public void resetEncoders() {
        m_leftEncoder.reset();
        m_rightEncoder.reset();
    }
    
    // The encoder positions returned here are relative to the
    // position that existed at the time the encoders were reset
    // They are ONLY meaningful after a reset has been done.
    // They are positive for positions "forward" of the reset
    // position and negative for positions "backward" of the reset
    // position.
    //
    // Note that while the *right* motor is inverted, it is
    // the *left* encoder that needs to be inverted to achieve
    // the above behavior.
    public double getLeftPosition() {
        return -m_leftEncoder.get();
    }

    public double getRightPosition() {
        return m_rightEncoder.get();
    }

    // The absolute position methods are intended for use *before* 
    // before the encoders are reset to begin normal operation.
    // They are used to move the arms to the starting position.
    public double getLeftAbsolutePosition() {
        return 1-m_leftEncoder.getAbsolutePosition();
    }

    public double getRightAbsolutePosition() {
        return m_rightEncoder.getAbsolutePosition()  + Constants.ClimberConstants.addToRightAbsPosition;
    }

    // Positive fraction corresponds to "increase encoder value"
    // for both sides.
    public void setMotorFractions(double leftFraction, double rightFraction) {
        if (DriveteamInterface.climberEnabled() && ((leftFraction != 0.0) || (rightFraction != 0.0))) {
            _releaseBrake();
            m_leftMotor.set(leftFraction);
            m_rightMotor.set(rightFraction);
        } else {
            stop();
        }
    }

    public void toggleBrake() {
        if (DriveteamInterface.climberEnabled()) {
            _toggleBrake();
        }
    }

    public void setBrake() {
        if (DriveteamInterface.climberEnabled()) {
            _setBrake();
        }
    }

    public void releaseBrake() {
        if (DriveteamInterface.climberEnabled()) {
            _releaseBrake();
        }
    }

    // These brake manipulation methods are private because they
    // don't check for enabled-ness. Anything that calls them 
    // must test for enabled-ness.
    private void _setBrake() {
        if (m_brakeSolenoid.get() !=  Value.kForward) {
            m_brakeSolenoid.set(Value.kForward);
        }
      }
    
    private void _releaseBrake() {
        if (m_brakeSolenoid.get() != Value.kReverse) {
            m_brakeSolenoid.set(Value.kReverse);
        }   
    }

    private void _toggleBrake() {
        m_brakeSolenoid.toggle();
    }

    public void stop() {
        m_leftMotor.set(0.0);
        m_rightMotor.set(0.0);
        // Important to use setBrake and not use _setBrake to avoid 
        // operation of the brake when climber is disabled.
        setBrake();
    }
}