package frc4061.robot.subsystems;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.TalonFXSimCollection;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.Pair;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.system.plant.DCMotor;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.simulation.FlywheelSim;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc4061.robot.Constants;
import frc4061.robot.Constants.ShooterConstants;
import frc4061.robot.DriveteamInterface;
import frc4061.robot.RobotMap;
import frc4061.robot.utilities.Conversions;
import frc4061.robot.utilities.SmartDashboardUtil;

public class Shooter extends SubsystemBase {
    private final WPI_TalonFX m_leftFlywheel;
    private final WPI_TalonFX m_rightFlywheel;
    private final CANSparkMax m_feederMotor;

    private final PIDController m_flywheelPID;
    private double m_flywheelVoltage;
    private double m_feederMotorFraction;

    // PID adjustment
    private double m_flywheelkP;
    private double m_flywheelkI;

    // On/off control
    private boolean m_flywheelOn = false;

    // Simulator support
    private final TalonFXSimCollection m_leftEncoderSim;
    private final TalonFXSimCollection m_rightEncoderSim;
    private final FlywheelSim m_flywheelSim;

    // SmartDashboard keys
    private final String d_temperatures = "Shooter/Left | Right Motor Temperatures";
    private final String d_flywheelRPM = "Shooter/Flywheel RPM";
    private final String d_flywheelVoltage = "Shooter/Flywheel Voltage";
    private final String d_currents = "Shooter/Left | Right Motor Currents";
    private final String d_flywheelP = "Shooter/Flywheel P";
    private final String d_flywheelI = "Shooter/Flywheel I";
    private final String d_targetDistance = "Shooter/Target Distance";
    private final String d_feederOutputSetpoint = "Shooter/Feeder output setpoint";
    private final String d_feederOutput = "Shooter/Feeder output";

    public Shooter() {
        m_leftFlywheel = new WPI_TalonFX(RobotMap.SHOOTER_LEFT);
        m_leftFlywheel.setNeutralMode(NeutralMode.Coast);
        m_leftFlywheel.setInverted(true);
        m_rightFlywheel = new WPI_TalonFX(RobotMap.SHOOTER_RIGHT);
        m_rightFlywheel.setNeutralMode(NeutralMode.Coast);
        m_rightFlywheel.follow(m_leftFlywheel);
        m_feederMotor = new CANSparkMax(RobotMap.FEEDER_MOTOR, MotorType.kBrushless);
        m_feederMotor.setIdleMode(IdleMode.kBrake);
        m_feederMotor.setInverted(true);

        m_flywheelPID = new PIDController(ShooterConstants.kP, ShooterConstants.kI, 0);
        m_flywheelPID.setTolerance(2);
        m_flywheelPID.setIntegratorRange(-200, 200);
        m_leftEncoderSim = m_leftFlywheel.getSimCollection();
        m_rightEncoderSim = m_rightFlywheel.getSimCollection();
        m_flywheelSim = new FlywheelSim(DCMotor.getFalcon500(2), ShooterConstants.kReduction, 2 * ShooterConstants.kMI);
        SmartDashboardUtil.ensureNumber(d_flywheelP, ShooterConstants.kP);
        m_flywheelkP = SmartDashboard.getNumber(d_flywheelP, ShooterConstants.kP);
        SmartDashboardUtil.ensureNumber(d_flywheelI, ShooterConstants.kI);
        m_flywheelkI = SmartDashboard.getNumber(d_flywheelI, ShooterConstants.kI);
        SmartDashboardUtil.ensureNumber(d_feederOutputSetpoint, ShooterConstants.kFeederOutputSetpoint);
    }

    private double calculateNewVoltage(double currentSpeed, double currentVoltage,
            PIDController pid) {
        var setpoint = pid.getSetpoint();
        var feedforward = ShooterConstants.kSVolt + setpoint * ShooterConstants.kVoltPerRPM;
        // the following allows voltage-based control (e.g. for characterization)
        // when the RPM setting is 0.
        if (setpoint == 0) {
            return currentVoltage;
        }
        /*if (currentSpeed < setpoint - 100)
            return 10.0; // max acceleration */
        var feedback = pid.calculate(currentSpeed);
        var candidate = feedforward + feedback;
        var limit = currentVoltage + ShooterConstants.kMaxStep;
        return MathUtil.clamp(Math.min(candidate, limit), -12, 12);
    }

    public void periodic() {
        double rpm = getRPM(); // Wheel speeds
        m_flywheelVoltage = calculateNewVoltage(rpm, m_flywheelVoltage, m_flywheelPID);

        if(DriveteamInterface.shooterEnabled()) {
            m_feederMotor.set(m_feederMotorFraction);
        } else {
            m_feederMotor.set(0);
        }

        if (DriveteamInterface.shooterEnabled() && m_flywheelOn) {
            m_leftFlywheel.setVoltage(m_flywheelVoltage);

            // this can be reimplemented once we are ready to have the feeder and flywheels linked
            //m_feederMotor.set(m_feederMotorFraction);
        } else {
            m_leftFlywheel.setVoltage(0);
            // this can be reimplemented once we are ready to have the feeder and flywheels linked
            //m_feederMotor.set(0);
            m_flywheelVoltage = 0;
        }

        SmartDashboard.putNumber(d_flywheelVoltage, m_flywheelVoltage);
        SmartDashboard.putString(d_temperatures, Conversions.pairToString(getTemperatures(), "°C"));
        SmartDashboard.putNumber(d_flywheelRPM, rpm);
        SmartDashboard.putNumber(d_feederOutput, m_feederMotorFraction);
        SmartDashboard.putString(d_currents, Conversions.pairToString(getCurrents(), "A"));
        // update distance to target
        var d = getDistance();
        SmartDashboard.putNumber(d_targetDistance, d);

        // Update kP, kI
        double kP = SmartDashboard.getNumber(d_flywheelP, ShooterConstants.kP);
        if (kP != m_flywheelkP) {
            m_flywheelkP = kP;
            m_flywheelPID.setP(kP);
        }
        double kI = SmartDashboard.getNumber(d_flywheelI, ShooterConstants.kI);
        if (kI != m_flywheelkI) {
            m_flywheelkI = kI;
            m_flywheelPID.setP(kI);
        }
    }

    public void setRPM(double flywheelRPM) {
        m_flywheelPID.setSetpoint(flywheelRPM);
    }

    public void setOutput(double flywheelVoltage) {
        m_flywheelVoltage = flywheelVoltage;
    }

    public double getRPM() {
        // These should both return the same velocity but we'll average them anyway
        return 0.5 * (m_leftFlywheel.getSelectedSensorVelocity() + m_rightFlywheel.getSelectedSensorVelocity())
                / ShooterConstants.sensorVelocityPerWheelRPM;
    }

    public double getVoltages() {
        return m_flywheelVoltage;
    }

    public void changeVoltage(double deltaVoltage) {
        m_flywheelVoltage += deltaVoltage;
    }

    // Call with true to turn it on, false for off
    public void setFlywheelOnOff(boolean on) {
        m_flywheelOn = on;
    }

    public boolean getFlywheelOnOff() {
        return m_flywheelOn;
    }

    public Pair<Double, Double> getCurrents() {
        return Pair.of(m_leftFlywheel.getStatorCurrent(), m_rightFlywheel.getStatorCurrent());
    }

    public Pair<Double, Double> getTemperatures() {
        return Pair.of(m_leftFlywheel.getTemperature(), m_rightFlywheel.getTemperature());
    }

    public void setGains(double flywheelP, double flywheelI) {
        m_flywheelPID.setPID(flywheelP, flywheelI, 0);
    }

    public void stopShooter() {
        setRPM(0);
        setOutput(0);
        toggleFeeder(false);
    }

    public void simulationPeriodic() {
        m_flywheelSim.setInputVoltage(m_flywheelVoltage);
        m_flywheelSim.update(Constants.kRobotPeriod);
        double velocity = m_flywheelSim.getAngularVelocityRPM();
        m_leftEncoderSim.setIntegratedSensorVelocity((int) (-velocity * ShooterConstants.sensorVelocityPerWheelRPM));
        m_rightEncoderSim.setIntegratedSensorVelocity((int) (velocity * ShooterConstants.sensorVelocityPerWheelRPM));
    }

    public double getCameraAngle() {
        return NetworkTableInstance.getDefault().getTable("limelight").getEntry("ty").getDouble(0.0);
    }

    public double getDistance() {
        double height = Constants.Limelight.targetHeight - Constants.Limelight.cameraHeight;
        double angleTanToTarget = Math.tan(Math.toRadians(Constants.Limelight.cameraAngle + getCameraAngle()));
        return height / angleTanToTarget;
    }

    public void toggleFeeder(boolean feed) {
        // once we find the right value for feederOutput, we should put it in constants and use that rather than SmartDashboard
        double feederOutput = SmartDashboard.getNumber(d_feederOutputSetpoint, 0);
        m_feederMotorFraction = feed ? feederOutput : 0;
    }
}
