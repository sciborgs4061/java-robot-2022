package frc4061.robot.subsystems;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.TalonFXSimCollection;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.hal.SimDouble;
import edu.wpi.first.hal.simulation.SimDeviceDataJNI;
import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.DifferentialDriveOdometry;
import edu.wpi.first.math.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.math.system.plant.DCMotor;
import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.simulation.DifferentialDrivetrainSim;
import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc4061.robot.Constants;
import frc4061.robot.DriveteamInterface;
import frc4061.robot.Robot;
import frc4061.robot.RobotMap;
import frc4061.robot.utilities.Conversions;

public class Drivebase extends SubsystemBase {
    private static final String leftEncodersKey = "Drivebase/Encoders/Left";
    private static final String rightEncodersKey = "Drivebase/Encoders/Right";
    private static final String leftEncoderMPSKey = "Drivebase/Encoders/Left m per s";
    private static final String rightEncoderMPSKey = "Drivebase/Encoders/Right m per s";
    private static final String leftVoltsKey = "Drivebase/Drive Volts - Left";
    private static final String rightVoltsKey = "Drivebase/Drive Volts - Right";
    private static final String currentKey = "Drivebase/Total Left | Right Current";
    private static final String temperatureKey = "Drivebase/Motor 0 Left | Right Temperature";
    
    // Driving and sensing
    private final WPI_TalonFX[] m_leftMotors;
    private final WPI_TalonFX[] m_rightMotors;
        
    public final AHRS m_navX;
    public final DifferentialDrive m_drive;
    

    // Odometry
    public final DifferentialDriveOdometry m_odometry;
    private final Field2d m_field;

    // Simulation
    private TalonFXSimCollection[] m_leftEncodersSim;
    private TalonFXSimCollection[] m_rightEncodersSim;
    private DifferentialDrivetrainSim m_dts;
    private SimDouble m_simAngle;

    public Drivebase() {
        // Motor Controllers
        m_leftMotors = motorIDsToMotors(RobotMap.LEFT_MOTOR_IDS, false);
        setupFollowers(m_leftMotors);

        m_rightMotors = motorIDsToMotors(RobotMap.RIGHT_MOTOR_IDS, true);
        setupFollowers(m_rightMotors);

        m_drive = new DifferentialDrive(m_leftMotors[0], m_rightMotors[0]);
        m_drive.setSafetyEnabled(true);
        m_drive.setExpiration(0.1);
        m_drive.setMaxOutput(1.0);

        // Odometry
        m_navX = new AHRS(SPI.Port.kMXP, Constants.Drivebase.kGyroUpdateRateHz);
        m_odometry = new DifferentialDriveOdometry(m_navX.getRotation2d(), new Pose2d(0, 0, new Rotation2d()));
        m_field = new Field2d();
        SmartDashboard.putData("Field", m_field);

        SmartDashboard.putNumber(leftEncodersKey, 0);
        SmartDashboard.putNumber(rightEncodersKey, 0);
        SmartDashboard.putNumber(leftEncoderMPSKey, 0);
        SmartDashboard.putNumber(rightEncoderMPSKey, 0);

        
        // Simulation - being careful not to instantiate this stuff on a real robot
        if (Robot.isSimulation()) {
            m_leftEncodersSim = motorsToSimCollection(m_leftMotors, false);
            m_rightEncodersSim = motorsToSimCollection(m_rightMotors, false);

            var dev = SimDeviceDataJNI.getSimDeviceHandle("navX-Sensor[0]");
            m_simAngle = new SimDouble((SimDeviceDataJNI.getSimValueHandle(dev, "Yaw")));
            m_dts = new DifferentialDrivetrainSim(DCMotor.getFalcon500(3), Constants.Drivebase.kGearRatio,
                    Constants.Drivebase.kMOI, Constants.Drivebase.kMass, Constants.Drivebase.kWheelDiameter/2, 
                    Constants.Drivebase.kNominalTrackWidthMeters, null);
        }
    }

    public double getLeftEncoderMeters() {
        return getEncoderMeters(m_leftMotors);
    }

    public double getRightEncoderMeters() {
        return getEncoderMeters(m_rightMotors);
    }

    /**
     * gets the average encoder distance in meters
     */
    private double getEncoderMeters(WPI_TalonFX[] motors) {
        double sum = 0.0;
        for (WPI_TalonFX m : motors) {
            sum += m.getSelectedSensorPosition();
        }     
        return nativeUnitsToDistanceMeters(sum / motors.length);
    }

    public double getLeftEncoderMetersPerSec() {
        return getEncoderMetersPerSec(m_leftMotors);
    }

    public double getRightEncoderMetersPerSec() {
        return getEncoderMetersPerSec(m_rightMotors);
    }

    public double getEncoderMetersPerSec(WPI_TalonFX[] motors) {
        double sum = 0.0;
        for (WPI_TalonFX m : motors) {
            sum += m.getSelectedSensorVelocity();
        }
        return nativeUnitsToMetersPerSecond(sum / motors.length);
    }

    @Override
    public void periodic() {
        // update predicted location
        m_odometry.update(m_navX.getRotation2d(), getLeftEncoderMeters(), getRightEncoderMeters());
        m_field.setRobotPose(m_odometry.getPoseMeters());

        SmartDashboard.putNumber(leftEncodersKey, getLeftEncoderMeters());
        SmartDashboard.putNumber(rightEncodersKey, getRightEncoderMeters());   
        SmartDashboard.putNumber(leftEncoderMPSKey, getLeftEncoderMetersPerSec());
        SmartDashboard.putNumber(rightEncoderMPSKey, getRightEncoderMetersPerSec());

        var left = m_leftMotors[0].get();   // just need 1 motor because others are following
        var right = m_rightMotors[0].get(); // ditto
        SmartDashboard.putNumber(leftVoltsKey, left);
        SmartDashboard.putNumber(rightVoltsKey, right);
        left = 3 * m_leftMotors[0].getStatorCurrent();    //estimate based on lead motor 
        right = 3 * m_rightMotors[0].getStatorCurrent();  //estimate based on lead motor 
        SmartDashboard.putString(currentKey, Conversions.twoToString(left, right, "A"));
        // "Temperature" is defined as "Controller Temperature", but since the controller
        // is integrated with the motor it ought to be closely related to the motor temp
        left = m_leftMotors[0].getTemperature();
        right = m_rightMotors[0].getTemperature();
        SmartDashboard.putString(temperatureKey, Conversions.twoToString(left, right, "°C"));
    }

    @Override
    public void simulationPeriodic() {
        var voltage = RobotController.getInputVoltage();
        var currHdg = m_dts.getHeading();

        m_dts.setInputs(m_leftMotors[0].get() * voltage,
                m_rightMotors[0].get() * voltage);
        m_dts.update(Constants.kRobotPeriod); // use time delta from last run to calc fake movements

        // fake out encoders
        setEncodersSim(m_leftEncodersSim, (m_dts.getLeftPositionMeters()), m_dts.getLeftVelocityMetersPerSecond());
        setEncodersSim(m_rightEncodersSim, -m_dts.getRightPositionMeters(), -m_dts.getRightVelocityMetersPerSecond());

        var hdgDiff = m_dts.getHeading().minus(currHdg);
        m_simAngle.set(m_simAngle.get() - hdgDiff.getDegrees());
    }

    public void stop() {
        m_drive.tankDrive(0.0, 0.0);
    }

    /**
     * Control the drive base by setting a speed and turning power.
     * 
     * @param speed    ranged [-1, 1]
     * @param turnRate ranged [-1, 1]
     */
    public void arcadeDrive(double speed, double turnRate) {
        if (DriveteamInterface.drivebaseEnabled())
            m_drive.arcadeDrive(speed, turnRate, true); // use squared-inputs setting
        else
            stop();
    }

    /**
     * Control the drive base by setting the power of either side's set of motors.
     * 
     * @param left  ranged [-1, 1]
     * @param right ranged [-1, 1]
     */
    public void tankDrive(double left, double right) {
        if (DriveteamInterface.drivebaseEnabled())
            m_drive.tankDrive(left, right, false);
        else
            stop();
    }

    /**
     * Control the drive base by setting the power of either side's set of motors
     * with volts
     * 
     * @param leftVolts  -12 .. +12 volts
     * @param rightVolts -12 .. +12 volts
     */
    public void tankDriveVolts(double leftVolts, double rightVolts) {
        if (DriveteamInterface.drivebaseEnabled()) {
            double voltage = RobotController.getBatteryVoltage();

            m_drive.tankDrive(MathUtil.clamp(leftVolts / voltage, -1.0, 1.0),
                    MathUtil.clamp(rightVolts / voltage, -1.0, 1.0), false);
        } else
            stop();
    }

    /**
     * Control the drive base by setting a speed power and a turning radius.
     * Turning behavior here differs from arcade drive in that it isn't affected by
     * the speed.
     * As a result curvature doesn't support zero point turns so this func sports a
     * deadband underwhich we instead use arcade drive to allow for ZPT.
     * 
     * @param speed      ranged [-1, 1]
     * @param turnRadius ranged [-1, 1] clockwise is pos
     */
    public void curvatureDrive(double speed, double turnRadius) {
        if (DriveteamInterface.drivebaseEnabled()) {
            m_drive.arcadeDrive(speed, turnRadius);
        } else
            stop();
    }

    public void resetOdometry(Pose2d pose) {
        resetEncoders();
        m_odometry.resetPosition(pose, m_navX.getRotation2d());

        if (Robot.isSimulation())
            m_dts.setPose(pose);
    }

    private void resetEncoders() {
        // resetEncoders is private because it should only be done in conjunction
        // with resetting the drive train simulator and odometry poses.
        // That is, use resetOdometry instead.
        for (WPI_TalonFX m : m_leftMotors) {
            m.setSelectedSensorPosition(0);
        }
        for (WPI_TalonFX m : m_rightMotors) {
            m.setSelectedSensorPosition(0);
        }
        if (Robot.isSimulation()) {
            setEncodersSim(m_leftEncodersSim, 0, 0);
            setEncodersSim(m_rightEncodersSim, 0, 0);
        }
    }

    public DifferentialDriveWheelSpeeds getWheelSpeeds() {
        return new DifferentialDriveWheelSpeeds(getLeftEncoderMetersPerSec(), getRightEncoderMetersPerSec());
    }

    ////////////////////////////////////////////////////
    // Private static helper functions \/
    ////////////////////////////////////////////////////

    private static final void setupFollowers(WPI_TalonFX[] motors) {
        motors[1].follow(motors[0]);
        motors[2].follow(motors[0]);
    }

    /**
     * return a factory reset brushless motor
     * 
     * @param CANBUSID the canbus id of the desired motor
     */
    private static final WPI_TalonFX[] motorIDsToMotors(Integer[] motorIDs, boolean reversed) {
        WPI_TalonFX[] mots = new WPI_TalonFX[motorIDs.length];

        for (int i = 0; i < motorIDs.length; i++) {
            mots[i] = getConfiguredMotor(motorIDs[i]);
            if (reversed)
                mots[i].setInverted(true);
        }

        return mots;
    }

    private static final WPI_TalonFX getConfiguredMotor(int CANBUSID) {
        WPI_TalonFX motor = new WPI_TalonFX(CANBUSID);
        // motor.clearFaults();
        // motor.restoreFactoryDefaults();

        motor.clearStickyFaults();
        motor.configFactoryDefault();
        motor.setNeutralMode(NeutralMode.Brake);

        return motor;
    }

    // For Simulated Motors/Encoders

    private static final TalonFXSimCollection[] motorsToSimCollection(WPI_TalonFX[] motors, boolean invert) {
        TalonFXSimCollection[] encs = new TalonFXSimCollection[motors.length];
        for (int i = 0; i < motors.length; i++) {
            encs[i] = motors[i].getSimCollection();
        }
        return encs;
    }

    
    /**
     * updates an array of simulated encoders with arbitrary values for position and
     * velocity
     */
    private static final void setEncodersSim(TalonFXSimCollection[] encodersSim, double position, double velocity) {
        for (TalonFXSimCollection e : encodersSim) {
            e.setIntegratedSensorRawPosition(distanceToNativeUnits(position));
            e.setIntegratedSensorVelocity(velocityToNativeUnits(velocity));
        }
    }

    private static final int distanceToNativeUnits(double positionMeters) {
        int sensorCounts = (int)(positionMeters / Constants.Drivebase.kDistancePerEncoderCount);
        return sensorCounts;
      }
    
    private static final int velocityToNativeUnits(double velocityMetersPerSecond) {
        // trying to do things here in a way that avoids premature truncation
        var sensorCountsPerSecond = (velocityMetersPerSecond / Constants.Drivebase.kDistancePerEncoderCount);
        int sensorCountsPer100ms = (int) (sensorCountsPerSecond / 10);
        return sensorCountsPer100ms;
    }

    private double nativeUnitsToDistanceMeters(double sensorCounts) {
        var positionMeters = sensorCounts * Constants.Drivebase.kDistancePerEncoderCount;
        return positionMeters;

    }

    private double nativeUnitsToMetersPerSecond(double sensorVelocity) {
        var velocityMetersPer100ms = sensorVelocity * Constants.Drivebase.kDistancePerEncoderCount;
        return velocityMetersPer100ms * 10;
    }
}
