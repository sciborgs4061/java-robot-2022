package frc4061.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc4061.robot.Constants;
import frc4061.robot.DriveteamInterface;
import frc4061.robot.RobotMap;
import frc4061.robot.utilities.SmartDashboardUtil;

public class Conveyor extends SubsystemBase {
    private CANSparkMax m_conveyorMotor;
    private DigitalInput m_upperBeamBreak;
    private DigitalInput m_lowerBeamBreak;
    private String upperStatus = "Upper Ball Present";
    private String lowerStatus = "Lower Ball Present";
    private String conveyor = "Conveyor/ ";

    public Conveyor() {
        m_conveyorMotor = new CANSparkMax(RobotMap.CONVEYOR_MOTOR, MotorType.kBrushless);
        m_conveyorMotor.setIdleMode(IdleMode.kBrake);
        m_conveyorMotor.setInverted(true);
        m_upperBeamBreak = new DigitalInput(RobotMap.UPPER_BEAM_BREAK_SENSOR);
        m_lowerBeamBreak = new DigitalInput(RobotMap.LOWER_BEAM_BREAK_SENSOR);
        SmartDashboardUtil.ensureBoolean(conveyor + upperStatus, false);
        SmartDashboardUtil.ensureBoolean(conveyor + lowerStatus, false);
    }

    public void periodic() {
        SmartDashboard.putBoolean(conveyor + upperStatus, upperBallPresent());
        SmartDashboard.putBoolean(conveyor + lowerStatus, lowerBallPresent());
        if (!DriveteamInterface.conveyorEnabled()) {
            stop();
        }
    }

    public boolean upperBallPresent() {
        return !m_upperBeamBreak.get();
    }

    public boolean lowerBallPresent() {
        return !m_lowerBeamBreak.get();
    }

    public void setSpeed(double speed) {
        m_conveyorMotor.set(DriveteamInterface.conveyorEnabled() ? speed : 0);
    }

    public void setIn() {
        setSpeed(Constants.Conveyor.conveyorSpeed);
    }

    public void setOut() {
        setSpeed(-Constants.Conveyor.conveyorSpeed);
    }

    public void stop() {
        setSpeed(0);
    }
}