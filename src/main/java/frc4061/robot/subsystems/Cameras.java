package frc4061.robot.subsystems;

import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.cscore.AxisCamera;
import edu.wpi.first.cscore.HttpCamera;
import edu.wpi.first.cscore.MjpegServer;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.cscore.VideoSource;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc4061.robot.DriveteamInterface;
import frc4061.robot.Constants.Limelight;

public class Cameras extends SubsystemBase {
    //private final MjpegServer m_camSW;
    private final AxisCamera m_frontCamera;
    private final HttpCamera m_limelightcamera;
 
    public Cameras() {
        super();
        m_frontCamera = CameraServer.addAxisCamera("IPCam354061", "10.40.61.35");
        m_limelightcamera = new HttpCamera("Limelight", "http://10.40.61.11:5800");
        CameraServer.addCamera(m_limelightcamera);
    }

    public static double getTarget(){
        SmartDashboard.putNumber("Target", NetworkTableInstance.getDefault().getTable("limelight").getEntry("tv").getDouble(0));
        return NetworkTableInstance.getDefault().getTable("limelight").getEntry("tv").getDouble(0);
    }
    
    @Override
    public void periodic () {
        getTarget();
    }
}

