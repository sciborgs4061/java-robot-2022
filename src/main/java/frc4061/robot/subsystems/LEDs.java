package frc4061.robot.subsystems;

import edu.wpi.first.wpilibj.AddressableLED;
import edu.wpi.first.wpilibj.AddressableLEDBuffer;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc4061.robot.RobotMap;

public class LEDs extends SubsystemBase {
    // you can only have one string and it can never change
    public static final AddressableLED m_leds = new AddressableLED(RobotMap.PWMLEDPort);
    public static final AddressableLEDBuffer m_ledBuffer = new AddressableLEDBuffer(RobotMap.nPWMLEDs);
    @Override
    public void periodic () {
        m_leds.setData(m_ledBuffer);
     }
    public LEDs() {
        register(); 
        m_leds.setLength(RobotMap.nPWMLEDs);
        m_leds.start();
    }
}
