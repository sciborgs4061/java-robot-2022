package frc4061.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.CANSparkMax.IdleMode;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc4061.robot.Constants;
import frc4061.robot.DriveteamInterface;
import frc4061.robot.RobotMap;

// A subsystem for the part of the robot that does
// azimuth and elevation manipulations.
public class Turret extends SubsystemBase {
    private final CANSparkMax m_elevationMotor;
    private final CANSparkMax m_azimuthMotor;

    private final RelativeEncoder m_elevationEncoder;
    private final RelativeEncoder m_azimuthEncoder;

    private final DigitalInput m_azimuthLeftLimit;
    private final DigitalInput m_azimuthRightLimit;
    private final DigitalInput m_elevationUpLimit;
    private final DigitalInput m_elevationDownLimit;
    private final NetworkTable m_limelight;

    public Turret() {
        m_elevationMotor = new CANSparkMax(RobotMap.ELEVATION_MOTOR, CANSparkMax.MotorType.kBrushless);
        m_elevationMotor.setIdleMode(IdleMode.kBrake);
        m_elevationMotor.setInverted(true);
        m_azimuthMotor = new CANSparkMax(RobotMap.AZIMUTH_MOTOR, CANSparkMax.MotorType.kBrushless);
        m_azimuthMotor.setIdleMode(IdleMode.kBrake);

        m_elevationEncoder = m_elevationMotor.getEncoder();
        m_elevationEncoder.setPositionConversionFactor(1.0);
        m_elevationEncoder.setVelocityConversionFactor(1.0);
        m_azimuthEncoder = m_azimuthMotor.getEncoder();
        m_azimuthEncoder.setPositionConversionFactor(1.0);
        m_azimuthEncoder.setVelocityConversionFactor(1.0);

        m_azimuthLeftLimit = new DigitalInput(RobotMap.AZIMUTH_LEFT_LIMIT);
        m_azimuthRightLimit = new DigitalInput(RobotMap.AZIMUTH_RIGHT_LIMIT);
        m_elevationUpLimit = new DigitalInput(RobotMap.ELEVATION_UP_LIMIT);
        m_elevationDownLimit = new DigitalInput(RobotMap.ELEVATION_DOWN_LIMIT);
        m_limelight = NetworkTableInstance.getDefault().getTable("limelight");
    }

    public void periodic() {
        SmartDashboard.putNumber("Shooter/Azimuth (encoder ticks)", m_azimuthEncoder.getPosition());
        SmartDashboard.putNumber("Shooter/Elevation (encoder ticks)", m_elevationEncoder.getPosition());
        SmartDashboard.putNumber("Shooter/CombinedElevation (deg)", 
            m_elevationEncoder.getPosition()/Constants.Turret.elevationTicksPerDegree + 
            m_limelight.getEntry("ty").getDouble(0));
        if (!DriveteamInterface.shooterEnabled()) {
            m_azimuthMotor.set(0);
            m_elevationMotor.set(0);
        }
    }

    public double rotation() {
        return m_azimuthEncoder.getPosition();
    }

    public double elevation() {
        return m_elevationEncoder.getPosition();
    }

    public boolean atLeftLimit() { return m_azimuthLeftLimit.get(); }

    public boolean atLowerLimit() { return m_elevationDownLimit.get(); }

    public boolean atRightLimit() { return m_azimuthRightLimit.get(); }

    public boolean atUpperLimit() { return m_elevationUpLimit.get(); }

    public boolean atLimits() {
        return atLeftLimit() && atLowerLimit();
    }

    public void resetEncoders() {
        m_azimuthEncoder.setPosition(0);
        m_elevationEncoder.setPosition(0);
    }

    // speed > 0: right
    public void rotate(double speed) {
        // if the motor isn't stopping, reverse the < > symbols
        speed = MathUtil.clamp(speed, -0.2, 0.2);
        if (DriveteamInterface.shooterEnabled())
            m_azimuthMotor
                    .set((m_azimuthLeftLimit.get() && speed < 0) || (m_azimuthRightLimit.get() && speed > 0) ? 0
                            : speed);
    }

    // speed > 0: up
    public void elevate(double speed) {
        speed = MathUtil.clamp(speed, -0.1, 0.1);
        if (DriveteamInterface.shooterEnabled())
            m_elevationMotor.set(
                    (m_elevationUpLimit.get() && speed > 0) || (m_elevationDownLimit.get() && speed < 0) ? 0 : speed);
    }
}
