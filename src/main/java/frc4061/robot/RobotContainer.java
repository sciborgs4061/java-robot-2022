/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc4061.robot;

import java.io.IOException;
import java.util.List;
import java.util.function.BooleanSupplier;
import java.util.function.DoubleSupplier;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.trajectory.TrajectoryGenerator;
import edu.wpi.first.math.trajectory.TrajectoryUtil;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.ParallelRaceGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc4061.robot.commands.AimShooter;
import frc4061.robot.commands.AlignTurret;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc4061.robot.commands.AlignTurret;
import frc4061.robot.commands.AllianceColor;
import frc4061.robot.commands.Azimuth;
import frc4061.robot.commands.BlinkLEDs;
import frc4061.robot.commands.BooleanDebuggerLEDCommand;
import frc4061.robot.commands.BooleanEvent;
import frc4061.robot.commands.Characterize;
import frc4061.robot.commands.CharacterizeShooter;
import frc4061.robot.commands.ClimberHome;
import frc4061.robot.commands.ClimberJoysticks;
import frc4061.robot.commands.Climb;
import frc4061.robot.commands.CollectorCollect;
import frc4061.robot.commands.CollectorReject;
import frc4061.robot.commands.CollectorToggleExtended;
import frc4061.robot.commands.ConveyorRejectBalls;
import frc4061.robot.commands.DisarmShooter;
import frc4061.robot.commands.DoubleDebuggerLEDCommand;
import frc4061.robot.commands.DriveNormal;
import frc4061.robot.commands.DriveVariableDistance;
import frc4061.robot.commands.Elevate;
import frc4061.robot.commands.ExitTarmac;
import frc4061.robot.commands.FeedShooter;
import frc4061.robot.commands.FollowTrajectoryFactory;
import frc4061.robot.commands.HomeTurret;
import frc4061.robot.commands.PongColorLEDs;
import frc4061.robot.commands.RunClimber;
import frc4061.robot.commands.RunConveyor;
import frc4061.robot.commands.RunFeeder;
import frc4061.robot.commands.RunShooter;
import frc4061.robot.commands.SetElevation;
import frc4061.robot.commands.ShooterAlignFixed;
import frc4061.robot.commands.ToggleFlyWheel;
import frc4061.robot.commands.WaveLEDs;
import frc4061.robot.subsystems.Cameras;
import frc4061.robot.subsystems.Climber;
import frc4061.robot.subsystems.Collector;
import frc4061.robot.subsystems.Conveyor;
import frc4061.robot.subsystems.Drivebase;
import frc4061.robot.subsystems.LEDSubset;
import frc4061.robot.subsystems.LEDs;
import frc4061.robot.subsystems.Shooter;
import frc4061.robot.subsystems.Turret;

/**
 * This class is where the bulk of the robot should be declared. Since
 * Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in
 * the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of
 * the robot
 * (including subsystems, commands, and button mappings) should be declared
 * here.
 */
public class RobotContainer {
  private static RobotContainer m_robotContainer = new RobotContainer();

  public static RobotContainer getInstance() {
    return m_robotContainer;
  }

  // The robot's subsystems and commands are defined here...
  public final Drivebase m_drivebase = new Drivebase();
  private final Shooter m_shooter = new Shooter();
  private final Collector m_collector = new Collector();
  private final Conveyor m_conveyor = new Conveyor();
  private final Climber m_climber = new Climber();
  private final Turret m_turret = new Turret();
  private final Cameras m_cameras = new Cameras();
  private final LEDs m_LEDs = new LEDs();
  private final LEDSubset m_leftFrontLEDs = new LEDSubset(0, 8, "Left Front LEDs");
  private final LEDSubset m_rightFrontLEDs = new LEDSubset(8, 16, "Right Front LEDs");
  private final LEDSubset m_rightBackLEDs = new LEDSubset(16, 24, "Right Back LEDs");
  private final LEDSubset m_leftBackLEDs = new LEDSubset(24, 32, "Left Back LEDs");

  BooleanSupplier[] bools = {
    ()->m_turret.atLeftLimit(), 
    ()->m_turret.atRightLimit(), 
    ()->m_turret.atUpperLimit(), 
    () -> m_turret.atLowerLimit(),
    () -> m_conveyor.lowerBallPresent(),
    () -> m_conveyor.upperBallPresent()
  };

  DoubleSupplier[] doubles = {
    () -> m_climber.getLeftAbsolutePosition(),
    () -> m_climber.getRightAbsolutePosition()
  };

  private final Command m_debugBooleanDIOs = new BooleanDebuggerLEDCommand(m_leftBackLEDs, bools);
  private final Command m_debugDoubleDIOs = new DoubleDebuggerLEDCommand(m_leftFrontLEDs, doubles);

  private final Command m_alliancecolorLeftFrontLEDs = new AllianceColor(m_leftFrontLEDs);
  private final Command m_alliancecolorLeftBackLEDs = new AllianceColor(m_leftBackLEDs);
  private final Command m_alliancecolorRightFrontLEDs = new AllianceColor(m_rightFrontLEDs);
  private final Command m_alliancecolorRightBackLEDs = new AllianceColor(m_rightBackLEDs);
  private final Command m_whiteLeftFrontLEDs = new BlinkLEDs(m_leftFrontLEDs, Color.kWhite);
  private final Command m_whiteLeftBackLEDs = new BlinkLEDs(m_leftBackLEDs, Color.kWhite);
  private final Command m_whiteRightFrontLEDs = new BlinkLEDs(m_rightFrontLEDs, Color.kWhite);
  private final Command m_whiteRightBackLEDs = new BlinkLEDs(m_rightBackLEDs, Color.kWhite);

  private final BooleanSupplier m_leftLimitSwitch = () -> m_turret.atLeftLimit();
  private final BooleanSupplier m_rightLimitSwitch = () -> m_turret.atRightLimit();
  private final BooleanSupplier m_upperLimitSwitch = () -> m_turret.atUpperLimit();
  private final BooleanSupplier m_lowerLimitSwitch = () -> m_turret.atLowerLimit();
  private final Command m_magentaBlinkLeftFrontLEDs = new BlinkLEDs(m_leftFrontLEDs, Color.kMagenta);
  private final Command m_magentaBlinkLeftBackLEDs = new BlinkLEDs(m_leftBackLEDs, Color.kMagenta);
  private final Command m_magentaBlinkRightFrontLEDs = new BlinkLEDs(m_rightFrontLEDs, Color.kMagenta);
  private final Command m_magentaBlinkRightBackLEDs = new BlinkLEDs(m_rightBackLEDs, Color.kMagenta);
  private final Command m_magentaPongLeftFrontLEDs = new PongColorLEDs(m_leftFrontLEDs, Color.kMagenta);
  private final Command m_magentaPongLeftBackLEDs = new PongColorLEDs(m_leftBackLEDs, Color.kMagenta);
  private final Command m_magentaPongRightFrontLEDs = new PongColorLEDs(m_rightFrontLEDs, Color.kMagenta);
  private final Command m_magentaPongRightBackLEDs = new PongColorLEDs(m_rightBackLEDs, Color.kMagenta);
  private final Command m_leftLimitSwitchHit = new BooleanEvent(m_leftLimitSwitch, false, m_magentaBlinkLeftFrontLEDs, m_magentaBlinkLeftBackLEDs);
  private final Command m_rightLimitSwitchHit = new BooleanEvent(m_rightLimitSwitch, false, m_magentaBlinkRightFrontLEDs, m_magentaBlinkRightBackLEDs);
  private final Command m_upperLimitSwitchHit = new BooleanEvent(m_upperLimitSwitch, false, m_magentaPongRightFrontLEDs, m_magentaPongRightBackLEDs);
  private final Command m_lowerLimitSwitchHit = new BooleanEvent(m_lowerLimitSwitch, false, m_magentaPongLeftFrontLEDs, m_magentaPongLeftBackLEDs);

  private final Command m_sciborgorangeLeftFrontLEDs = new WaveLEDs(m_leftFrontLEDs, Color.kOrange);
  private final Command m_sciborgorangeLeftBackLEDs = new WaveLEDs(m_leftBackLEDs, Color.kOrange);
  private final Command m_sciborgorangeRightFrontLEDs = new WaveLEDs(m_rightFrontLEDs, Color.kOrange);
  private final Command m_sciborgorangeRightBackLEDs = new WaveLEDs(m_rightBackLEDs, Color.kOrange);
  private final Command m_purpleLeftFrontLEDs = new WaveLEDs(m_leftFrontLEDs, Color.kMediumPurple);
  private final Command m_purpleLeftBackLEDs = new WaveLEDs(m_leftBackLEDs, Color.kMediumPurple);
  private final Command m_purpleRightFrontLEDs = new WaveLEDs(m_rightFrontLEDs, Color.kMediumPurple);
  private final Command m_purpleRightBackLEDs = new WaveLEDs(m_rightBackLEDs, Color.kMediumPurple);
  private final Command m_yellowLeftFrontLEDs = new WaveLEDs(m_leftFrontLEDs, Color.kGoldenrod);
  private final Command m_yellowrLeftBackLEDs = new WaveLEDs(m_leftBackLEDs, Color.kGoldenrod);
  private final Command m_yellowRightFrontLEDs = new WaveLEDs(m_rightFrontLEDs, Color.kGoldenrod);
  private final Command m_yellowRightBackLEDs = new WaveLEDs(m_rightBackLEDs, Color.kGoldenrod);
  //front LEDs means climer goes forward, back means it goes backwards. vvv
  private final Command m_paleGoldenrodLeftFrontLEDs = new WaveLEDs(m_leftFrontLEDs,  Color.kPaleGoldenrod);
  private final Command m_paleGoldenrodLeftBackLEDs = new WaveLEDs(m_leftBackLEDs,  Color.kPaleGoldenrod);
  private final Command m_paleGoldenrodRightFrontLEDs = new WaveLEDs(m_rightFrontLEDs,  Color.kPaleGoldenrod);
  private final Command m_paleGoldenrodRightBackLEDs = new WaveLEDs(m_rightBackLEDs,  Color.kPaleGoldenrod);
  // private final Command m_whiteLeftLEDs = new SolidColorLEDs(m_leftLEDs, Color.kWhite);
  // private final Command m_steelblueLeftLEDs = new PongColorLEDs(m_leftLEDs, Color.kSteelBlue);
  // private final Command m_greenLeftLEDs = new SolidColorLEDs(m_leftLEDs, Color.kGreen);
  // private final Command m_goldRightLEDs = new WaveLEDs(m_rightLEDs, Color.kGold);
  // private final Command m_purpleRightLEDs = new SolidColorLEDs(m_rightLEDs, Color.kPurple); 
  // private final Command m_redRightLEDs = new SolidColorLEDs(m_rightLEDs, Color.kRed); 
  private final Command m_runConveyor = new RunConveyor(m_conveyor);
  private final Command m_runClimber = new ClimberJoysticks(m_climber);
  private final Command m_driveNormal = new DriveNormal(m_drivebase);
  private final RunShooter m_runShooter = new RunShooter(m_shooter); // note RunShooter extends Command
  private final Command m_azimuthLeft = new Azimuth(m_turret, -Constants.Turret.speed);
  private final Command m_azimuthRight = new Azimuth(m_turret, Constants.Turret.speed);
  private final Command m_shooterUp = new Elevate(m_turret, Constants.Turret.speed);
  private final Command m_shooterDown = new Elevate(m_turret, -Constants.Turret.speed);
  private final Command m_runFeeder = new RunFeeder(m_shooter);
  private final Command m_collectorCollect = new CollectorCollect(m_collector);
  private final Command m_disarmShooter = new DisarmShooter(m_conveyor);
  private final Command m_feedShooter =  new FeedShooter(m_conveyor);
  private final Command m_spinOutCollector = new CollectorReject(m_collector);
  private final Command m_waitFeedShooter = new SequentialCommandGroup(new WaitCommand(0.5),
                                                                      m_feedShooter);
  private final Command m_waitConveyorRejectBalls = new SequentialCommandGroup(new WaitCommand(0.5),
                                                                              new ConveyorRejectBalls(m_conveyor));
  private final Command m_rejectBall = new ParallelCommandGroup(m_spinOutCollector,
                                                               m_waitConveyorRejectBalls);
  private final Command m_shoot = new ParallelCommandGroup(m_waitFeedShooter,
                                                          new ToggleFlyWheel(m_shooter),
                                                          m_runFeeder);
  
  private final Command m_alignTurret = new AlignTurret(m_turret);
  // AlignTurret (azimuth-only) replaced by AimShooter (Azimuth, Elevation, Speed)
  private final Command m_aimShooter = new AimShooter(m_turret, m_runShooter);
  private final FollowTrajectoryFactory m_factory = new FollowTrajectoryFactory(m_drivebase);
  private final Command m_driveVariableDistanceCmd = new DriveVariableDistance(m_drivebase, m_factory);
  private final Command m_exitTarmac = new ExitTarmac(m_drivebase, m_factory);
  private final Command m_toggleCollectorExtended = new CollectorToggleExtended(m_collector);
  private final SendableChooser<Command> m_chooser = new SendableChooser<>();
   private final Command m_fenderAim = new SequentialCommandGroup(
      new ShooterAlignFixed(m_turret, m_shooter, 37.45, 56.35, 1600),
      // override the RPM setting of ShooterAlignFixed with a voltage setting
      new InstantCommand(()->m_runShooter.setShooterVoltageSetpoint(6.273))
    );

  // for 2 ball center
  private final Command m_driveToCenterBall = m_factory.getFollowerForPathweaverJson("2BallCenter1.wpilib.json");
  private final Command m_driveFromCenterBallToTerminalBall = m_factory.getFollowerForPathweaverJson("2BallCenter2.wpilib.json");

  // for 1 ball center
  private final Command m_driveToCenterBallOnly = m_factory.getFollowerForPathweaverJson("2BallCenter1.wpilib.json");

  // for 2 ball left
  private final Command m_driveToLeftBall = m_factory.getFollowerForPathweaverJson("2BallLeft1.wpilib.json");
  private final Command m_driveFromLeftBallToTerminalBall = m_factory.getFollowerForPathweaverJson("2BallLeft2.wpilib.json");

  // for 2 ball right
  private final Command m_driveToRightBall = m_factory.getFollowerForPathweaverJson("2BallRight1.wpilib.json");
  private final Command m_driveFromRightBallToTerminalBall = m_factory.getFollowerForPathweaverJson("2BallRight2.wpilib.json");

  // for 1 ball left
  private final Command m_driveToLeftBallOnly = m_factory.getFollowerForPathweaverJson("2BallLeft1.wpilib.json");
  private final Command m_driveLeftBit = m_factory.getFollowerForPathweaverJson("2BallLeftBit.wpilib.json");

  // for 1 ball right
  private final Command m_driveToRightBallOnly = m_factory.getFollowerForPathweaverJson("2BallRight1.wpilib.json");
  private final Command m_driveRightTurnBit = m_factory.getFollowerForPathweaverJson("2BallRightBit.wpilib.json");

  // for 2 ball right tarmac
  private final Command m_driveToRightBallFirst = m_factory.getFollowerForPathweaverJson("2BallRight1.wpilib.json");
  private final Command m_driveFromRightBallToTarmacBall = m_factory.getFollowerForPathweaverJson("2BallRightTarmac2.wpilib.json");

  // for single ball auto
  private final Command m_driveForward = m_factory.getFollowerForPathweaverJson("DriveForward.wpilib.json");

  Command TwoBallRight = new ParallelRaceGroup(
                              new CollectorCollect(m_collector), 
                              new SequentialCommandGroup(autoShoot(43.8, 15.6, 1825, "2BallRight1.wpilib.json", "2BallRight2.wpilib.json"),
                                                         m_driveFromRightBallToTerminalBall));

  Command OneBallRight = new ParallelRaceGroup(
                              new CollectorCollect(m_collector),
                              new SequentialCommandGroup(autoShoot(43.8, 15.6, 1825, "2BallRight1.wpilib.json", "2BallRightBit.wpilib.json"),
                                                         m_driveRightTurnBit));

  Command TwoBallCenter = new ParallelRaceGroup(
                                new CollectorCollect(m_collector), 
                                new SequentialCommandGroup(autoShoot(74, 9, 2100, "2BallCenter1.wpilib.json", null),
                                                           m_driveFromCenterBallToTerminalBall));

  Command OneBallCenter = new ParallelRaceGroup(
                                new CollectorCollect(m_collector), 
                                autoShoot(74, 9, 2075, "2BallCenter1.wpilib.json", null));

  Command TwoBallLeft = new ParallelRaceGroup(
                                new CollectorCollect(m_collector), 
                                new SequentialCommandGroup(autoShoot(65.8, 15.8, 1900, "2BallLeft1.wpilib.json", null),
                                                           m_driveFromLeftBallToTerminalBall));

  Command OneBallLeft = new ParallelRaceGroup(
                                new CollectorCollect(m_collector), 
                                new SequentialCommandGroup(autoShoot(65.8, 15.8, 1900, "2BallLeft1.wpilib.json", null),
                                                            m_driveLeftBit));
  
  Command TwoBallRightTarmac = new ParallelRaceGroup(
                                      new CollectorCollect(m_collector),
                                      new SequentialCommandGroup(autoShoot(43.8, 15.6, 1825, "2BallRight1.wpilib.json", "2BallRightTarmac2.wpilib.json"),
                                                                 m_driveFromRightBallToTarmacBall));

  Command SingleBall = new ParallelCommandGroup(m_driveForward);

  /**
   * The container for the robot. Contains subsystems, OI devices, and commands.
   */
  private RobotContainer() {

    configureButtonBindings();

    m_leftLimitSwitchHit.schedule();
    m_rightLimitSwitchHit.schedule();
    m_upperLimitSwitchHit.schedule();
    m_lowerLimitSwitchHit.schedule();

    m_leftFrontLEDs.setDefaultCommand(m_alliancecolorLeftFrontLEDs);
    m_leftBackLEDs.setDefaultCommand(m_alliancecolorLeftBackLEDs);
    m_rightFrontLEDs.setDefaultCommand(m_alliancecolorRightFrontLEDs);
    m_rightBackLEDs.setDefaultCommand(m_alliancecolorRightBackLEDs);

    m_shooter.setDefaultCommand(m_runShooter);
    m_drivebase.setDefaultCommand(m_driveNormal);
    // m_leftLEDs.setDefaultCommand(m_steelblueLeftLEDs);
    // m_rightLEDs.setDefaultCommand(m_goldRightLEDs);
    m_conveyor.setDefaultCommand(m_runConveyor);
    m_climber.setDefaultCommand(m_runClimber);
    // LED Examples
    // m_aButton.whenHeld(m_redRightLEDs);
    // m_bButton.whenPressed(m_greenLeftLEDs.withTimeout(5));

  
    /////////////////////////////////////// START LEDs ////////////////////////////////////////////////////
    DriveteamInterface.Driver.m_climberForwardButton.whenHeld(m_paleGoldenrodLeftFrontLEDs);
    DriveteamInterface.Driver.m_climberForwardButton.whenHeld(m_paleGoldenrodRightFrontLEDs);
    DriveteamInterface.Driver.m_climberBackwardButton.whenHeld(m_paleGoldenrodLeftBackLEDs);
    DriveteamInterface.Driver.m_climberBackwardButton.whenHeld(m_paleGoldenrodRightBackLEDs);

    DriveteamInterface.Operator.m_rejectBallButton.whenHeld(m_yellowLeftFrontLEDs);
    DriveteamInterface.Operator.m_rejectBallButton.whenHeld(m_yellowRightFrontLEDs);
    DriveteamInterface.Operator.m_rejectBallButton.whenHeld(m_yellowrLeftBackLEDs);
    DriveteamInterface.Operator.m_rejectBallButton.whenHeld(m_yellowRightBackLEDs);

    DriveteamInterface.Operator.m_collectBallButton.whenHeld(m_purpleLeftFrontLEDs);
    DriveteamInterface.Operator.m_collectBallButton.whenHeld(m_purpleRightFrontLEDs);
    DriveteamInterface.Operator.m_collectBallButton.whenHeld(m_purpleLeftBackLEDs);
    DriveteamInterface.Operator.m_collectBallButton.whenHeld(m_purpleRightBackLEDs);

    DriveteamInterface.Operator.m_shootButton.whenPressed(m_sciborgorangeLeftFrontLEDs);
    DriveteamInterface.Operator.m_shootButton.whenPressed(m_sciborgorangeRightFrontLEDs);
    DriveteamInterface.Operator.m_shootButton.whenPressed(m_sciborgorangeLeftBackLEDs);
    DriveteamInterface.Operator.m_shootButton.whenPressed(m_sciborgorangeRightBackLEDs);

    // DriveteamInterface.Robot.m_debugButton.whenHeld(m_debugBooleanDIOs);
    // DriveteamInterface.Robot.m_debugButton.whenHeld(m_debugDoubleDIOs);
    DriveteamInterface.Robot.m_debugButton.whenHeld(new ParallelCommandGroup (
      m_debugBooleanDIOs,
      m_debugDoubleDIOs,
      new AllianceColor(m_rightBackLEDs),
      new AllianceColor(m_rightFrontLEDs)
    ));
    ////////////////////////////////////// END LEDs //////////////////////////////////////////////
    
    DriveteamInterface.Driver.m_climberForwardButton.whenHeld(new RunClimber(m_climber, 0.25));
    DriveteamInterface.Driver.m_climberBackwardButton.whenHeld(new RunClimber(m_climber, -0.25));

    DriveteamInterface.Operator.m_azimuthLeftButton.whenHeld(m_azimuthLeft);
    DriveteamInterface.Operator.m_azimuthRightButton.whenHeld(m_azimuthRight);
    DriveteamInterface.Operator.m_elevationUpButton.whenHeld(m_shooterUp);
    DriveteamInterface.Operator.m_elevationDownButton.whenHeld(m_shooterDown);
    DriveteamInterface.Operator.m_fenderAimButton.whenHeld(m_fenderAim);
    DriveteamInterface.Operator.m_shootButton.whenHeld(m_shoot);
    DriveteamInterface.Driver.m_collectBallButton.whenHeld(m_collectorCollect);
    DriveteamInterface.Driver.m_toggleCollectorButton.whenPressed(m_toggleCollectorExtended);
    DriveteamInterface.Driver.m_rejectBallButton.whenHeld(m_rejectBall);
    DriveteamInterface.Driverstation.m_climberBrakeToggle.whenPressed(
      new InstantCommand(() -> m_climber.releaseBrake(), m_climber).perpetually().withName("Release brake")
      );
    DriveteamInterface.Driverstation.m_climberBrakeToggle.whenReleased(
      new InstantCommand(() -> m_climber.setBrake(), m_climber)
      );
    DriveteamInterface.Operator.m_alignShooterButton.whenHeld(m_aimShooter);
    
    Command climberJoysticks = new ClimberJoysticks(m_climber);
    Command retractCollector = new InstantCommand(m_collector::retract);
    DriveteamInterface.Driverstation.m_homeAllSubsystems.whenHeld(
      (retractCollector
      .alongWith(new HomeTurret(m_turret)))
      // assumes climber is already in home position
      .alongWith(new InstantCommand(()->m_climber.resetEncoders()))
      .withName("Homing")
      );
    // TODO: Add Collector and Turret default commands using a command group on this button
    DriveteamInterface.Driverstation.m_homeAllSubsystems.whenReleased(
      () -> m_climber.setDefaultCommand(climberJoysticks)
      ); 

    DriveteamInterface.Driverstation.m_incrementShooter.whenPressed(
      () -> m_runShooter.adjustShooterRPMsetpoint(50)
      );

    DriveteamInterface.Driverstation.m_decrementShooter.whenPressed(
      () -> m_runShooter.adjustShooterRPMsetpoint(-50)
      );
    
    m_chooser.addOption("Characterize Drivebase", new Characterize(m_drivebase));

    m_chooser.addOption("Characterize Shooter", new CharacterizeShooter(m_shooter));
    m_chooser.addOption("Drive Variable Distance", m_driveVariableDistanceCmd);
  
    m_chooser.addOption("2-Ball Center", TwoBallCenter);
    m_chooser.addOption("1-Ball Center", OneBallCenter);
    m_chooser.addOption("2-Ball Left", TwoBallLeft);
    m_chooser.addOption("1-Ball Left", OneBallLeft);
    m_chooser.addOption("2-Ball Right", TwoBallRight);
    m_chooser.addOption("1-Ball Right", OneBallRight);
    m_chooser.addOption("2-Ball Right Tarmac", TwoBallRightTarmac);
    m_chooser.addOption("Exit Tarmac", m_exitTarmac);
    m_chooser.addOption("SingleBall", SingleBall);
    SmartDashboard.putData("Auto Command", m_chooser);
    DriveteamInterface.initialize(); // puts the DriveteamInterfaces entries on SmartDashboard
  }

  class DisabledInstantCommand extends InstantCommand {
    DisabledInstantCommand(Runnable r) {
      super(r);
    }

    @Override
    public boolean runsWhenDisabled() {
      return true;
    }
  }

  /**
   * Use this method to define your button->command mappings. Buttons can be
   * created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing
   * it to a
   * {@link edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */

  private void configureButtonBindings() {
    // Unused - did not work well enough to be useful - try to resurrect later

  }

  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    return m_chooser.getSelected();
  }

  public Command autoShoot(double azimuth, double elevation, double rpm, String path, String successorPath) {
    var feedShooter =  new FeedShooter(m_conveyor);
    var align = new ShooterAlignFixed(m_turret, m_shooter, elevation, azimuth, rpm).withTimeout(5);
    var runFeeder = new RunFeeder(m_shooter);
    Trajectory initialTrajectory;
    Trajectory successorTrajectory;
    try {     
      initialTrajectory = TrajectoryUtil.fromPathweaverJson(Filesystem.getDeployDirectory().toPath().resolve("paths/" + path));
    } catch (IOException ex) {
       DriverStation.reportError("Unable to open trajectory: " + path, ex.getStackTrace());
       return null; // FIX THIS - return an LED command to flash red or some other alarm indicator
    }

    var follower = m_factory.getFollower(initialTrajectory);
    var driveAndAlignShooter = new ParallelCommandGroup(follower, align);
    Command connector;
    if (successorPath != null) {
      try {     
        successorTrajectory = TrajectoryUtil.fromPathweaverJson(Filesystem.getDeployDirectory().toPath().resolve("paths/" + successorPath));
      } catch (IOException ex) {
         DriverStation.reportError("Unable to open trajectory: " + successorPath, ex.getStackTrace());
         return null; // FIX THIS - return an LED command to flash red or some other alarm indicator
      }
      Pose2d finalPose = initialTrajectory.sample(initialTrajectory.getTotalTimeSeconds()).poseMeters;
      Pose2d successorInitialPose = successorTrajectory.getInitialPose();
      
      var connectorTraj = TrajectoryGenerator.generateTrajectory(
        List.of(finalPose, successorInitialPose),
        Constants.Drivebase.configReversed
      );
      connector = m_factory.getFollower(connectorTraj);
    } else {
      connector = new InstantCommand(() -> {});
    }
    var shoot = new SequentialCommandGroup(
      new HomeTurret(m_turret).withTimeout(2), // 2 seconds may be longer than we need, but under normal circumstances we should never hit the timeout
      new InstantCommand(() -> m_shooter.setFlywheelOnOff(true)),  
      driveAndAlignShooter,  
      new WaitCommand(SmartDashboard.getNumber("Autonomous Parameters/Auto first shot delay", 0)),
      new ParallelCommandGroup( feedShooter, runFeeder).withTimeout(0.75),
      new InstantCommand(() -> m_shooter.setFlywheelOnOff(false)),
      connector
    );
    // Allow this command to take over control of the shooter from the 
    // default RunShooter command.
    shoot.addRequirements(m_shooter);
   

    return shoot;
  }
}
